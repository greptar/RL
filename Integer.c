#if defined(__GNUC__)
#define SCCS SCCS __attribute__ ((__unused__))
#endif

static char *SCCS = "@(#) Integer.c (1.1), Last modified: 14:53:46 13 Sep 1995";

/* ------------------------------------------------------------------------- */

#if defined(NeXT)
#include <stdlib.h>		/* where malloc() is defined on the NeXT */
#else
#include <malloc.h>		/* where malloc() is defined elsewhere   */
#endif

#include <stdio.h>		/* where NULL is defined on SunOS */

#include <RL/Array.h>
#include <RL/Integer.h>
#include <RL/TS.h>

/* ----------------------------------------------------------------------------------------------------------------------------- */

Integer_Type *
Integer_Allocate(void)
{
  return (malloc(sizeof(Integer_Type)));
}

void
Integer_Deallocate(Integer_Type *Integer)
{
  free(Integer);
}

/* ----------------------------------------------------------------------------------------------------------------------------- */

Integer_Type *
Integer_Read(Integer_Type *Integer, String_Type Name, SF_Type SF)
{
  if (Integer == NULL)
  {
    Integer_Type *New_Integer = Integer_Allocate();

    if (SF_Get_Integer(SF, Name, New_Integer))
      return (New_Integer);
  }
  else
  {
    if (SF_Get_Integer(SF, Name, Integer))
      return (Integer);
  }

  return (NULL);
}

Boolean_Type
Integer_Write(Integer_Type *Integer, String_Type Name, SF_Type SF)
{
  return (SF_Put_Integer(SF, Name, *Integer));
}

/* ----------------------------------------------------------------------------------------------------------------------------- */

Integer_1D_Type
Integer_1D_Allocate(Integer_Type Length)
{
  return ((Integer_1D_Type) Array_1D_Allocate(Length, sizeof(Integer_Type)));
}

void
Integer_1D_Deallocate(Integer_1D_Type Array)
{
  Array_1D_Deallocate((Array_1D_Type) Array);
}

/* ----------------------------------------------------------------------------------------------------------------------------- */

Integer_2D_Type
Integer_2D_Allocate(Integer_Type Length_1, Integer_Type Length_2)
{
  return ((Integer_2D_Type) Array_2D_Allocate(Length_1, Length_2, sizeof(Integer_Type)));
}

void
Integer_2D_Deallocate(Integer_2D_Type Array)
{
  Array_2D_Deallocate((Array_2D_Type) Array);
}

/* ----------------------------------------------------------------------------------------------------------------------------- */

Integer_3D_Type
Integer_3D_Allocate(Integer_Type Length_1, Integer_Type Length_2, Integer_Type Length_3)
{
  return ((Integer_3D_Type) Array_3D_Allocate(Length_1, Length_2, Length_3, sizeof(Integer_Type)));
}

void
Integer_3D_Deallocate(Integer_3D_Type Array)
{
  Array_3D_Deallocate((Array_3D_Type) Array);
}

/* ----------------------------------------------------------------------------------------------------------------------------- */

String_Type
Integer_To_String(Integer_Type Value)
{
  String_Type Result;
  static
    TS_Type   TS = NULL;

  if (TS == NULL)
    TS = TS_Allocate(8);

  sprintf(Result = TS_Item(TS, 32), "%d", Value);

  return (Result);
}

