/* SCCS: @(#) Network.h (1.1), Last modified: 14:53:04 13 Sep 1995 */

#if !defined(NETWORK_H)
#define NETWORK_H

/* ----------------------------------------------------------------------------------------------------------------------------- */

/*
   CONCEPTS:

   These routines establish network (either server or client) connections.  To
   perform actual IO over the network, use the routines found in IO.h.

*/

/* ----------------------------------------------------------------------------------------------------------------------------- */

#include <RL/IO.h>

/* ----------------------------------------------------------------------------------------------------------------------------- */

typedef enum
{
  Iterative,			/* The server doesn't fork    */
  Replicating			/* The server forks a process */
} Network_Server_Type;

/* ----------------------------------------------------------------------------------------------------------------------------- */

extern
IO_Descriptor_Type
Network_Allocate_Server_Port(Integer_Type Port_Number);
/*
   Returns a port that will be used to make either a replicating or iterative
   server.  This port cannot be used for reads or writes.  Returns
   NULL if there is a failure.
*/


/*
   WARNING: THIS PACKAGE USES THE SIGC[H]LD SIGNAL!!!  DO NOT USE IT!
            THIS WILL BE REMOVED SOMETIME IN THE FUTURE.
*/

/* maybe allow for a timeout on an iterative server */
extern
IO_Type
Network_Allocate_Server(IO_Descriptor_Type  Server_Base,
			Network_Server_Type Type_Of_Server);
/*
   DESCRIPTION
     Accepts network connections.  The Server is either Replicating or
     Iterative.

     If the Server is Replicating, a new process is created for each network
     connection.  The child process returns from this function while the
     parent never returns.  The single duty of the parent is to accept
     incoming network connections and create a child process to respond to the
     connection.

     If the Server is Iterative, the function blocks until a connection is
     made.  When a connection is made, the function returns immediately.
     Unlike the Replicating Server, the Iterative Server never creates new
     processes.

   RETURN VALUE
     An IO Channel upon which IO operations can be performed across the
     network.  If there is a problem with the connection, NULL is returned.

   PARAMETERS
     The Server Base, obtained from Network_Allocate_Server_Port, and the type
     of network Server.
*/

extern
IO_Type
Network_Connect_To_Server(String_Type Host, Integer_Type Port_Number);
/*
   Returns an active conduit connected to the given server.  Returns NULL if
   there is a failure.
*/

#endif  /* NETWORK_IO_H */
