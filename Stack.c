#if defined(__GNUC__)
#define SCCS SCCS __attribute__ ((__unused__))
#endif

static char *SCCS = "@(#) Stack.c (1.1), Last modified: 14:53:36 13 Sep 1995";

/* ------------------------------------------------------------------------- */

#if defined(NeXT)
#include <stdlib.h>		/* where malloc() is defined on the NeXT */
#else
#include <malloc.h>		/* where malloc() is defined elsewhere   */
#endif

#include <stdarg.h>
#include <stdio.h>

#include <RL/List.h>
#include <RL/Stack.h>

/* ----------------------------------------------------------------------------------------------------------------------------- */

typedef struct _Stack_Type
{
  List_Type                 Stack;
  Stack_Deallocate_Delegate Deallocate;
} _Stack_Type;

/* ----------------------------------------------------------------------------------------------------------------------------- */

extern
Stack_Type
Stack_Allocate(Delegate_Type Delegate, ...)
{
  va_list    Argument_Pointer;
  Stack_Type Stack;

  Stack             = (Stack_Type) malloc(sizeof(_Stack_Type));
  Stack->Stack      = List_Allocate(No_Delegates);
  Stack->Deallocate = NULL;

  va_start(Argument_Pointer, Delegate);

  do
  {
    if (Delegate == NULL_DELEGATE)
    {
      break;
    }
    else
    {
      switch (Delegate)
      {
	Generic_Type Dummy;

      case DEALLOCATE_DELEGATE:
	Stack->Deallocate = va_arg(Argument_Pointer, Generic_Type);
	break;
      default:			/* Don't process other delegates */
	Dummy = va_arg(Argument_Pointer, Generic_Type);
	break;
      }
    }
  } while ((Delegate = va_arg(Argument_Pointer, Delegate_Type)) != NULL_DELEGATE);

  va_end(Argument_Pointer);

  return (Stack);
}

/* ----------------------------------------------------------------------------------------------------------------------------- */

extern
void
Stack_Deallocate(Stack_Type Stack)
{
  Stack_Clear(Stack);
  List_Deallocate(Stack->Stack);
  free(Stack);
}

/* ----------------------------------------------------------------------------------------------------------------------------- */

extern
void
Stack_Clear(Stack_Type Stack)
{
  Stack_Item_Type Item;

  if (Stack->Deallocate)
    while ((Item = Stack_Pop(Stack)) != NULL)
      Stack->Deallocate(Item);
  else
    List_Clear(Stack->Stack);
}

/* ----------------------------------------------------------------------------------------------------------------------------- */

extern
void
Stack_Push(Stack_Type Stack, Stack_Item_Type Item)
{
  List_Prepend_Item(Stack->Stack, Item);
}

/* ----------------------------------------------------------------------------------------------------------------------------- */

extern
Stack_Item_Type
Stack_Pop(Stack_Type Stack)
{
  return (List_Remove_Item_At_Head(Stack->Stack));
}

/* ----------------------------------------------------------------------------------------------------------------------------- */

extern
Stack_Item_Type
Stack_Top(Stack_Type Stack)
{
  return (List_Item_At_Head(Stack->Stack));
}

/* ----------------------------------------------------------------------------------------------------------------------------- */

extern
Boolean_Type
Stack_Is_Empty(Stack_Type Stack)
{
  return (List_Is_Empty(Stack->Stack));
}

/* ----------------------------------------------------------------------------------------------------------------------------- */

extern
Integer_Type
Stack_Size(Stack_Type Stack)
{
  return (List_Length(Stack->Stack));
}
