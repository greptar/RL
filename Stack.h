/* SCCS: @(#) Stack.h (1.1), Last modified: 14:52:35 13 Sep 1995 */

#if !defined(STACK_H)
#define STACK_H

#include <RL/Common_Delegates.h>
#include <RL/SF.h>

typedef struct _Stack_Type *Stack_Type;
typedef void               *Stack_Item_Type;


typedef void (*Stack_Deallocate_Delegate)(Stack_Item_Type Item);


extern
Stack_Type
Stack_Allocate(Delegate_Type Delegate, ...); /* There can be only one ... */

extern
void
Stack_Deallocate(Stack_Type Stack);
/*
   Deallocates the stack and all items on the stack.
*/

extern
void
Stack_Clear(Stack_Type Stack);
/*
   If a Deallocate Delegate is defined, it is called to free each item in the stack
   and the stack size is set to zero.
*/

extern
void
Stack_Push(Stack_Type Stack, Stack_Item_Type Item);
/*
   Puts an item on the stack.
*/

extern
Stack_Item_Type
Stack_Pop(Stack_Type Stack);
/*
   Returns NULL if the stack is empty, otherwise it returns (and removes from the stack)
   the most recently pushed item.  It does *NOT* deallocate the item for you.  If you
   don't want this item, you have to throw it away yourself.
*/

extern
Stack_Item_Type
Stack_Top(Stack_Type Stack);
/*
   Returns NULL if the stack is empty, otherwise it returns the top item.
*/

extern
Boolean_Type
Stack_Is_Empty(Stack_Type Stack);

extern
Integer_Type
Stack_Size(Stack_Type Stack);

#endif /* STACK_H */
