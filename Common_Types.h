/* SCCS: @(#) Common_Types.h (1.2), Last modified: 16:49:59 05 Oct 1995 */

#if !defined(COMMON_TYPES_H)
#define COMMON_TYPES_H

/* ----------------------------------------------------------------------------------------------------------------------------- */

/*
   X11 Bogosity: X is full of "bad" programming.  This section tries to undo
                 them before it screws things up.  Please skip past this section.
		 I wish someone would rewrite X to make it more palatteable.

   Bogosity #1 & #2:
     The values "False" and "True" are #defined to be "0" and "1".  Not only
     do I despise the practice of #defining, but this violates the established
     convention of preprocessor tokens being ALL UPPERCASE.  Besides, the
     values for False and True are much better served by using an enumerated
     data type since the debugger with print their values in symbolic form.

   Bogosity #3:
     Someone #defined "Status" to be "int", which makes it very *inconvenient*
     to declare a status variable: "int Status;" becomes "int int".  Well, I
     would much rather be able to *read and maintain* my software than abort my
     style and name my variables bogus, non-descriptive names.  I really wish
     you couldn't (or wouldn't?) do dirty stupid things with the preprocessor.
*/

#if defined(False)		/* Bogosity #1 */
#undef False
#endif

#if defined(True)		/* Bogosity #2 */
#undef True
#endif

#if defined(Status)		/* Bogosity #3 */
#undef Status
#endif

/* ----------------------------------------------------------------------------------------------------------------------------- */

/*
   These types are declared here due to dependency problems.  They used to be
   declared in the "correct" place, such as Booleans in Boolean.h, but when
   the Save File routines were added, circular dependencies developed.  The
   operations for these types are still defined in the correct place.

   What, exactly, was the problem?  The Save File needed to know about the
   common data types, such as String_Type, and #included String.h to pickup
   the type declaration.  At the same time, String.h defined the prototype for
   String_Read and String_Write, both of which received the Save File data
   type SF_Type.  Therefore, String.h included SF.h and SF.h included String.h,
   which made for a very unpleasant situation.  The result is this file.
*/

typedef enum { False, True } Boolean_Type;

typedef char Character_Type;

typedef float         Float_Type; 	/* A 32-bit float? */
typedef Float_Type   *Float_1D_Type;
typedef Float_Type  **Float_2D_Type;
typedef Float_Type ***Float_3D_Type;

typedef double         Double_Type;	/* A 64-bit float? */
typedef Double_Type   *Double_1D_Type;
typedef Double_Type  **Double_2D_Type;
typedef Double_Type ***Double_3D_Type;

typedef void *Generic_Type;

typedef int             Integer_Type;
typedef Integer_Type   *Integer_1D_Type;
typedef Integer_Type  **Integer_2D_Type;
typedef Integer_Type ***Integer_3D_Type;

typedef char *String_Type;


/* #ifdef wrap the following section for different architectures */

#if defined(sun) || defined(sgi) || defined(next)

typedef          char       Byte_Type; /*  8-bits,   signed */
typedef unsigned char      uByte_Type; /*  8-bits, unsigned */
typedef          short int  Word_Type; /* 16-bits,   signed */
typedef unsigned short int uWord_Type; /* 16-bits, unsigned */
typedef          int        Long_Type; /* 32-bits,   signed */
typedef unsigned int       uLong_Type; /* 32-bits, unsigned */

#else

add architecture here (intentional compilation termination) !!!

#endif

#endif /* COMMON_TYPES_H */
