#if defined(__GNUC__)
#define SCCS SCCS __attribute__ ((__unused__))
#endif

static char *SCCS = "@(#) IO.c (1.1), Last modified: 14:53:48 13 Sep 1995";

/* ------------------------------------------------------------------------- */

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>

#include <sys/types.h>		/* mrg */
#include <sys/stat.h>		/* mrg */

#include <RL/IO.h>
#include <RL/List.h>
#include <RL/String.h>

typedef struct _IO_Type
{
  IO_Connection_Type  Type;
  FILE               *Stream;
  IO_Descriptor_Type  Descriptor;
  Generic_Type        Start_of_Map;
  Generic_Type        Current_Location_in_Map;

  Byte_Type          *Buffer;	       /* mrg */
  Byte_Type          *Buffer_Position; /* mrg */
  Integer_Type        Buffer_Size;     /* mrg */
} _IO_Type;

/****************************************************************************/

static List_Type IO_Buffer_List = NULL;

void
IO_Buffer_Cleanup(void)
{
  List_Reset_Cursor(IO_Buffer_List);

  while (List_Move_Cursor_Forward(IO_Buffer_List))
    IO_Close(List_Item_At_Cursor(IO_Buffer_List));

  List_Deallocate(IO_Buffer_List);
}

void
IO_Buffer_Allocate(IO_Type X)
{
  struct stat statbuf;		/* mrg */

  if (IO_Buffer_List == NULL)
  {
    IO_Buffer_List = List_Allocate(No_Delegates);
    atexit(IO_Buffer_Cleanup);
  }

  fstat(X->Descriptor, &statbuf);
  IO_Printf(IO_stdout, "bufsiz = %d for %d\n", statbuf.st_blksize, X->Descriptor);
  X->Buffer = malloc(statbuf.st_blksize);
  X->Buffer_Size = statbuf.st_blksize;
  X->Buffer_Position = X->Buffer;

  List_Append_Item(IO_Buffer_List, X);
}

/*  Integer_Type Number_Of_Bytes = X->Buffer_Position + N; */

Integer_Type
IO_Buffer_Output(IO_Type X, Byte_Type *B, Integer_Type N)
{
  Integer_Type Total = N;

  while (N > 0)
  {
    Integer_Type Bytes_Available = (X->Buffer + X->Buffer_Size) - X->Buffer_Position;
    /*X->Buffer_Size - (X->Buffer_Position + N);*/

    if (Bytes_Available <= N)
    {
      memcpy(X->Buffer_Position, B, Bytes_Available);
      write(X->Descriptor, X->Buffer, X->Buffer_Size);
      B += Bytes_Available;
      X->Buffer_Position = X->Buffer;
      N -= Bytes_Available;
    }
    else
    {
      memcpy(X->Buffer_Position, B, N);
      X->Buffer_Position += N;
      N = 0;
    }
  }

  return (Total);
}


#if 0
    N -= Bytes_Available;

    if (N > 0)
    {
      /* write them */
    }
    else
    {
      /* they are buffered */
    }
    
    X->Buffer_Position += N;


    if (X->Buffer_Position + N < X->Buffer_Size)
    {
      memcpy(&X->Buffer[X->Buffer_Position], B, N);
      X->Buffer_Position += N;
    }
    else
    {
      memcpy(&X->Buffer[X->Buffer_Position], B, X->Buffer_Size - (X->Buffer_Position + N));
      X->Buffer_Position += N;
    }

#endif

/****************************************************************************/

extern int errno;

static
void
Error(String_Type Error_Message)
{
  char error_message[128];
  
  strcpy(error_message, "IO.");
  strcat(error_message, Error_Message);
  
  perror(error_message);
}


IO_Type
IO_Standard_Input(void)
{
  static IO_Type Standard_In = NULL;

  if(!Standard_In)
    return (Standard_In = IO_Allocate(Stream, stdin));
  else
    return (Standard_In);
}


IO_Type
IO_Standard_Output(void)
{
  static IO_Type Standard_Out = NULL;

  if(!Standard_Out)
    return (Standard_Out = IO_Allocate(Stream, stdout));
  else
    return (Standard_Out);
}


IO_Type
IO_Standard_Error(void)
{
  static IO_Type Standard_Error = NULL;

  if(!Standard_Error)
    return (Standard_Error = IO_Allocate(Stream, stderr));
  else
    return (Standard_Error);
}


IO_Type
IO_Allocate(IO_Connection_Type Connection_Type, ...)
{
  va_list args;
  IO_Type New_Conduit = (IO_Type) malloc(sizeof(_IO_Type));

  New_Conduit->Type = Connection_Type;

  va_start(args, Connection_Type);
  
  switch (Connection_Type)
  {
  case Socket:
  case Stream:
    New_Conduit->Stream     = va_arg(args, FILE *);
    New_Conduit->Descriptor = fileno(New_Conduit->Stream);
    fprintf(stderr, "IO_Allocate: Descriptor = %d\n", New_Conduit->Descriptor);
    if (Connection_Type == Socket)
      IO_Buffer_Allocate(New_Conduit);
    break;

  default:
    IO_Printf(IO_stderr, "Only can do sockets and streams for now\n");
    exit(1);
  }
  
  va_end(args);
  
  return (New_Conduit);
}


Integer_Type
IO_Read(IO_Type      Conduit,
	Generic_Type Buffer,
	Integer_Type Length )
{
  Integer_Type Result;
  
  if (!Conduit)
  {
    IO_Printf(IO_stderr, "Conduit sent to IO_Read is NULL\n");
    return (0);
  }

  switch (Conduit->Type)
  {
  case Socket:
  case Stream:
    Result = fread(Buffer, 1, Length, Conduit->Stream);
    break;
  default:
    IO_Printf(IO_stderr, "Conduit sent to IO_Read has bad type\n");
    Result = 0;
    break;
  }

#if 0
  if (Conduit->Type == Socket)
  {
    IO_Flush(Conduit);
  }
#endif

  return (Result);
}  


Integer_Type
IO_Write(IO_Type      Conduit,
	 Generic_Type Buffer,
	 Integer_Type Length )
{
  Integer_Type Result;
  
  if (!Conduit)
  {
    IO_Printf(IO_stderr, "Conduit sent to IO_Write is NULL\n");
    return (0);
  }
  
  switch (Conduit->Type)
  {
  case Socket:
    Result = IO_Buffer_Output(Conduit, Buffer, Length);
    break;
  case Stream:
    Result = fwrite(Buffer, 1, Length, Conduit->Stream);
    break;
  default:
    IO_Printf(IO_stderr, "Conduit sent to IO_Write has bad type\n");
    Result = 0;
    break;
  }

#if 0
  if (Conduit->Type == Socket)
  {
    IO_Flush(Conduit);
  }
#endif

  return (Result);
}


void
IO_Close(IO_Type Conduit)
{
  if (!Conduit)
  {
    IO_Printf(IO_stderr, "Conduit sent to IO_Close is NULL\n");
    return;
  }

  switch (Conduit->Type)
  {
  case Socket:
    IO_Flush(Conduit);
    fclose(Conduit->Stream);	/* this will change */
    free(Conduit->Buffer);
    List_Reset_Cursor(IO_Buffer_List);
    while (List_Move_Cursor_Forward(IO_Buffer_List))
      if (Conduit == List_Item_At_Cursor(IO_Buffer_List))
	List_Remove_Item_At_Cursor(IO_Buffer_List);
    break;
  case Stream:
    fclose(Conduit->Stream);
    break;
  case Memory_Mapped:
    /* do things to close up a memory mapped file here */
    break;
  }

  free(Conduit);
}


void
IO_Flush(IO_Type Conduit)
{
  if (!Conduit)
  {
    IO_Printf(IO_stderr, "Conduit sent to IO_Flush is NULL\n");
    return;
  }

  switch (Conduit->Type)
  {
  case Socket:
    write(Conduit->Descriptor, Conduit->Buffer, Conduit->Buffer_Position - Conduit->Buffer);
    Conduit->Buffer_Position = Conduit->Buffer;
    break;
  case Stream:
    fflush(Conduit->Stream);
    break;
  case Memory_Mapped:
    /* what would flushing a memory mapped file do? */
    break;
  }
}


Integer_Type
IO_Printf(IO_Type      Conduit,
	  String_Type  Format,
	  ...)
{
  va_list      args;
  Integer_Type Result;
  char         Buffer[1024];
  
  if (!Conduit)
  {
    IO_Printf(IO_stderr, "Conduit sent to IO_Printf is NULL\n");
    return (0);
  }

  if (!Format)
  {
    Result = 0;
  }
  else
  {
    va_start(args, Format);
    Result = vsprintf(Buffer, Format, args);

#if defined(sun)		/* this sucks vsprintf returns Buffer on a Sun */
    Result = strlen(Buffer);
#endif

#if 0
    fprintf(stderr, "Result = %d\n", Result);
    fprintf(stderr, "Buffer = %d\n", Buffer);
#endif

    if (Conduit->Type == Socket)
      Result = IO_Buffer_Output(Conduit, Buffer, Result); /* write(fileno(Conduit->Stream), Buffer, Result);*/
    else
      Result = fwrite(Buffer, sizeof(char), Result, Conduit->Stream);

    va_end(args);
  }
  
#if 0
  if (Conduit->Type == Socket)
  {
    IO_Flush(Conduit);
  }
#endif

  return (Result);
}


String_Type
IO_Gets(String_Type 	Array_to_Fill,
	Integer_Type 	Max_Number_of_Bytes_to_Read,
	IO_Type		Conduit)
{
  String_Type Result;
  
  if (!Conduit)
  {
    IO_Printf(IO_stderr, "Conduit sent to IO_Gets is NULL\n");
    return (NULL);
  }

  Result = fgets(Array_to_Fill, Max_Number_of_Bytes_to_Read, Conduit->Stream);
  
#if 0
  if (Conduit->Type == Socket)
  {
    IO_Flush(Conduit);
  }
#endif

  return (Result);
}


/* Should this (and other) routines deallocate the IO_Type on EOF ??? */
/* Probably not -- stdio doesn't do this and the data stream could grow. */
String_Type
IO_Get_Line(IO_Type Conduit)
{
  char         Buffer[1024];
  Integer_Type Length;
  
  if (!Conduit)
  {
    return (NULL);
  }
  if ((fgets(Buffer, 1024, Conduit->Stream)) == NULL)
  {
    return (NULL);
  }

  if ((Length = strlen(Buffer)) == 1023)
  {
    IO_Printf(IO_stderr, "Line longer than 1023 truncated by IO_Get_Line");
  }
  else if (!feof(Conduit->Stream))
  {
    Buffer[Length - 1] = '\0';
  }

  if (Buffer[Length - 2] == '\r')
  {
    Buffer[Length - 2] = '\0';
  }

#if 0
  if (Conduit->Type == Socket)
  {
    IO_Flush(Conduit);
  }
#endif

  return (String_Duplicate(Buffer));
}


Integer_Type
IO_Put_Line(IO_Type Conduit, String_Type String)
{
  static const
    Character_Type  New_Line_Character = '\n';
  Integer_Type      Result;
  Integer_Type      String_Length;

  if (!Conduit)
  {
    IO_Printf(IO_stderr, "Conduit sent to IO_Put_Line is NULL\n");
    return (-1);
  }
  else if (!String)
  {
    return (0);
  }
  else if ((Conduit->Type != Socket) && (Conduit->Type != Stream))
  {
    IO_Printf(IO_stderr, "Conduit sent to IO_Put_Line has bad type\n");
    return (0);
  }

  String_Length = strlen(String);

  if (Conduit->Type == Socket)
    Result = IO_Buffer_Output(Conduit, String, String_Length);
  else
    Result = fwrite(String, sizeof(char), String_Length, Conduit->Stream);

  if (Result != String_Length)
  {
    Error("IO_Put_Line(IO_Write)");
    return (-1);
  }

  /* We don't want to do this if it already has a new line ??? */

  if (Conduit->Type == Socket)
    Result = IO_Buffer_Output(Conduit, &New_Line_Character, 1);
  else
    Result = fwrite(&New_Line_Character, sizeof(char), 1, Conduit->Stream);

  if (Result != 1)
  {
    Error("IO_Put_Line(IO_Write(New_Line))");
    return (-1);
  }
  else
  {
    Result++;
  }

#if 0
  if (Conduit->Type == Socket)
  {
    IO_Flush(Conduit);
  }
#endif

  return (Result);
}


IO_Descriptor_Type
IO_Descriptor_Of(IO_Type Handle)
{
  return (Handle->Descriptor);
}
