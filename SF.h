/* SCCS: @(#) SF.h (1.1), Last modified: 14:52:42 13 Sep 1995 */

#if !defined(SAVE_FILE_H)
#define SAVE_FILE_H

/*
   Save File
*/

/* ------------------------------------------------------------------------ */

typedef struct _SF_Type *SF_Type;

/* ------------------------------------------------------------------------ */

#include <RL/Common_Types.h>

/* ------------------------------------------------------------------------ */

extern
SF_Type
SF_Allocate(void);

extern
void
SF_Deallocate(SF_Type SF);

/* ------------------------------------------------------------------------ */

extern
Boolean_Type
SF_Load(SF_Type Save_File, String_Type Filename);
/*
   If Filename does not end with ".sf", the extension is automatically
   appended.
*/

extern
Boolean_Type
SF_Save(SF_Type Save_File, String_Type Filename);
/*
   If Filename does not end with ".sf", the extension is automatically
   appended.
*/

/* ------------------------------------------------------------------------ */

extern
Boolean_Type
SF_Put_Comment(SF_Type Save_File, String_Type Comment);
/*
   Adds a comment (for reader documentation) in the save file.
*/

/* ------------------------------------------------------------------------ */

/*
   The "Put" routines return True if successful, False if not.
*/

extern
Boolean_Type
SF_Put_Character(SF_Type Save_File, String_Type Name, Character_Type Value);

extern
Boolean_Type
SF_Put_String(SF_Type Save_File, String_Type Name, String_Type Value);

extern
Boolean_Type
SF_Put_Enumeration(SF_Type Save_File, String_Type Name, String_Type Value);

extern
Boolean_Type
SF_Put_Integer(SF_Type Save_File, String_Type Name, Integer_Type Value);

extern
Boolean_Type
SF_Put_Float(SF_Type Save_File, String_Type Name, Float_Type Value);

extern
SF_Type
SF_Begin_Collection(SF_Type Save_File, String_Type Name);
/*
   Returns NULL if invalid.  Never free or SF_Deallocate the
   returned save file.
*/

extern
Boolean_Type
SF_End_Collection(SF_Type Save_File);
/*
   Don't use the Save_File (the collection) after calling this routine.
*/

/* ------------------------------------------------------------------------ */

/*
   The "Get" routines return True if successful, False if not.
*/

extern
Boolean_Type
SF_Get_Character(SF_Type Save_File, String_Type Name, Character_Type *Value);

extern
Boolean_Type
SF_Get_String(SF_Type Save_File, String_Type Name, String_Type *Value);

extern
Boolean_Type
SF_Get_Enumeration(SF_Type Save_File, String_Type Name, String_Type *Value);

extern
Boolean_Type
SF_Get_Integer(SF_Type Save_File, String_Type Name, Integer_Type *Value);

extern
Boolean_Type
SF_Get_Float(SF_Type Save_File, String_Type Name, Float_Type *Value);

extern
Boolean_Type
SF_Get_Collection(SF_Type Save_File, String_Type Name, SF_Type *Value);
/*
   Never free or SF_Deallocate the returned save file.
*/

#endif /* SAVE_FILE_H */
