#if defined(__GNUC__)
#define SCCS SCCS __attribute__ ((__unused__))
#endif

static char *SCCS = "@(#) Boolean.c (1.1), Last modified: 14:53:53 13 Sep 1995";

/* ------------------------------------------------------------------------- */

#if defined(NeXT)
#include <stdlib.h>		/* where malloc() is defined on the NeXT */
#else
#include <malloc.h>		/* where malloc() is defined elsewhere   */
#endif

#if defined(sun)
#include <stdio.h>		/* where NULL is defined on SunOS */
#endif

#include <string.h>

#include <RL/Boolean.h>

/* ------------------------------------------------------------------------ */

Boolean_Type *
Boolean_Allocate(void)
{
  return (malloc(sizeof(Boolean_Type)));
}

void
Boolean_Deallocate(Boolean_Type *Boolean)
{
  free(Boolean);
}

/* ------------------------------------------------------------------------ */

Boolean_Type *
Boolean_Read(Boolean_Type *Boolean, String_Type Name, SF_Type SF)
{
  String_Type S;

  if (Boolean == NULL)
  {
    Boolean_Type *New_Boolean = Boolean_Allocate();

    if (SF_Get_Enumeration(SF, Name, &S))
    {
      if (strcmp(S, "True") == 0)
      {
	*New_Boolean = True; return (New_Boolean);
      }
      else if (strcmp(S, "False") == 0)
      {
	*New_Boolean = False; return (New_Boolean);
      }
    }
  }
  else
  {
    if (SF_Get_Enumeration(SF, Name, &S))
    {
      if (strcmp(S, "True") == 0)
      {
	*Boolean = True; return (Boolean);
      }
      else if (strcmp(S, "False") == 0)
      {
	*Boolean = False; return (Boolean);
      }
    }
  }
  
  return (NULL);
}


Boolean_Type
Boolean_Write(Boolean_Type *Boolean, String_Type Name, SF_Type SF)
{
  return (SF_Put_Enumeration(SF, Name, *Boolean == False ? "False" : "True"));
}
