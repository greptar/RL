/* SCCS: @(#) TS.h (1.1), Last modified: 14:52:30 13 Sep 1995 */

#if !defined(TS_H)
#define TS_H

#include <RL/Common_Types.h>

/*
  TS => Temporary Storage (I couldn't think of a better name).

  TS maintains a circular list of memory locations for temporary storage
  designed to be used inside functions which return local static vars.

  For example, the following maintains a local char buffer which it returns:

    char *itoa(int i)
    {
      static char buf[80];
      sprintf(buf, "%d", i);
      return (buf);
    }

  This could lead to problems with nested function calls like:

    foo(itoa(m), itoa(n));

  The same value gets passed to foo (either the itoa(m) or the itoa(n)).  A
  better approach is:

    char *itoa(int i)
    {
      static TS_Type My_TS = NULL;
      char *r;
      if (My_TS == NULL)
        My_TS = TS_Allocate(4);
      sprintf(r = TS_Item(My_TS, 80), "%d", i);
      return (r);
    }

  which would allow four simultaneous nested calls to itoa(), such as:

    foo(itoa(a), itoa(b), itoa(c), itoa(d));

  This approach also removes side affects where foo() calls itoa() and you
  call foo(itoa(n)) without realizing that foo() also calls itoa().
*/

typedef struct _TS_Type *TS_Type;

extern
TS_Type
TS_Allocate(Integer_Type Number_Of_Items);

extern
void
TS_Deallocate(TS_Type TS);

extern
Generic_Type
TS_Item(TS_Type TS, Integer_Type Size_Of_Item);

#endif /* TS_H */
