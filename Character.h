/* SCCS: @(#) Character.h (1.1), Last modified: 14:53:19 13 Sep 1995 */

#if !defined(CHARACTER_H)
#define CHARACTER_H

#include <RL/SF.h>

/* ------------------------------------------------------------------------ */

extern
Character_Type *
Character_Allocate(void);

extern
void
Character_Deallocate(Character_Type *Character);

/* ------------------------------------------------------------------------ */

extern
Character_Type *
Character_Read(Character_Type *Character, String_Type Name, SF_Type SF);

extern
Boolean_Type
Character_Write(Character_Type *Character, String_Type Name, SF_Type SF);

/*
   If Name is an empty string, it is part of a nameless collection.
*/

#endif /* CHARACTER_H */
