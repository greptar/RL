/* SCCS: @(#) List.h (1.2), Last modified: 16:49:58 05 Oct 1995 */

#if !defined(LIST_H)
#define LIST_H

/* ----------------------------------------------------------------------------------------------------------------------------- */

/*
   CONCEPTS:
     A List is a collection of Items.  The List contains methods to allocate
     new Lists, deallocate or clear existing Lists, traverse the List, examine
     Items it the List, add and delete Items in the List, etc.

     An Item is a generic pointer (void *) to a user-defined data structure.
     The user is responsible for allocating space (by using a memory allocator
     such as malloc) before adding the Item to the List.

     The List is responsible for maintaining the data added to the List.  The
     List can also maintain data (to a degree) that is removed from the List.
     When a List is created, a memory deallocation procedure is passed in
     called Free_Item.  Free_Item can be a NULL pointer if memory deallocation
     is not desired or it can be the library routine "free" for simple
     components or a user-defined routine for more advanced components, such
     as those with pointers in them.  Whenever the user removes an Item from
     the List a check is made to see if the Item should be deallocated from
     memory.  Regardless of whether or not the List Items are deleted, the List
     entry (or possibly even the list) will.

     List Positions range from 1 <= Position <= List_Length, where List_Length
     is the number of Items in the List.  If there are zero Items in the List,
     the List is empty and there are no Positions.

     The Head of the List is Position 1, the Tail of the List is position N.

     A Cursor can be used to navigate the List.  A Cursor is similar to the
     Cursor in a terminal; it represents a Position in the List and can be
     moved forwards and backwards.  Cursors can also be used to retrieve
     Items, delete Items, insert Items, etc.  The Cursor Position tends to
     follow the Item the Cursor is pointing at, unless a deletion occurs at
     the Cursor.

     An Iterator exists to navigate over each Item in the List.  The Iterator
     calls a user-defined function for each Item and allows processing to be
     performed for each Item in List order (Head to Tail).  The List should not
     be modified while iterating.
*/

/* ----------------------------------------------------------------------------------------------------------------------------- */

#include <RL/Common_Delegates.h>
#include <RL/SF.h>

/* ----------------------------------------------------------------------------------------------------------------------------- */

/*
   Things that should be added/modified:
     reverse lists
     List_Contains
     List_Sort (?)
     List_Insert

     How about: List_Type List_Intersection(List1, List2);
                List_Type List_Union(List1, List2);
                List_Type List_Difference(List1, List2);  ???
     These would require a Compare Delegate and would give a type of Set capability.
*/     

/* ----------------------------------------------------------------------------------------------------------------------------- */

typedef struct _List_Type *List_Type;
typedef Generic_Type       List_Item_Type;
typedef Generic_Type       List_Label_Type;
typedef Integer_Type       List_Position_Type;

/* ----------------------------------------------------------------------------------------------------------------------------- */

typedef Integer_Type   (*List_Compare_Delegate)   (List_Label_Type Left, List_Item_Type Right);
typedef void           (*List_Deallocate_Delegate)(List_Item_Type Item);
typedef List_Item_Type (*List_Duplicate_Delegate) (List_Item_Type Item);
typedef Generic_Type   (*List_Read_Delegate)      (List_Item_Type Item, String_Type Name, SF_Type Save_File);
typedef Boolean_Type   (*List_Write_Delegate)     (List_Item_Type Item, String_Type Name, SF_Type Save_File);


extern
List_Type
List_Allocate(Delegate_Type Delegate, ...);
/*
   DESCRIPTION
     Creates and initializes a new List instance containing zero (0) Items.
     This routine must be called before any List operations are performed on
     the List instance.  There is no programmatic limit on the number of List
     instances available.

   RETURN VALUE
     The new List instance.  This value is a required parameter in all
     subsequent List instance operations.  If a List cannot be allocated,
     NULL is returned.

   PARAMETERS
     A NULL terminated list of Delegates.

     Deallocate Delegate:
       The Deallocate Delegate is used to deallocate the dynamic memory of an
       Item, but not the List instance itself.  If the Item is a simple Item which
       does not contain pointers to additional Items, the C Library routine
       "free()" can be used.  If the Item is a compound Item which contains
       pointers to additional Items, the delegate must be a user-defined
       routine which releases all dynamic memory referenced within the object
       and then deallocate the Item itself.  The Deallocate Delegate *always*
       deallocates the Item and if the Item is compound, it must deallocate
       the Items within the Item.

     Read Delegate:
     Write Delegate:
*/

extern
void
List_Deallocate(List_Type List);
/*
   DESCRIPTION
     Deallocates a List instance.  All Items in the List are freed from memory,
     if a Deallocate Delegate exists, and removed from the List.  After the
     Items are removed, the List is deallocated from memory making further
     operations on the List invalid.

   RETURN VALUE
     None.

   PARAMETERS
     The List instance.
*/

/* ----------------------------------------------------------------------------------------------------------------------------- */

extern
void
List_Prepend_Item(List_Type List, List_Item_Type Item);
/*
   DESCRIPTION
     Places an Item at the Head of the List.

     If the Cursor is being used, it continues to point to the same Item and
     the Cursor Position is incremented.


   RETURN VALUE
     None.

   PARAMETERS
     The List instance.
*/

extern
void
List_Append_Item(List_Type List, List_Item_Type Item); 
/*
   DESCRIPTION
     Places an Item at the Tail of the List.

     If the Cursor is being used, it continues to point to the same Item and
     the Cursor Position is not incremented.

   RETURN VALUE
     None.

   PARAMETERS
     The List instance.
*/

extern
void
List_Insert_Item_At_Position(List_Type List, List_Item_Type Item, List_Position_Type Position);

extern
void
List_Insert_Item_At_Cursor(List_Type List, List_Item_Type Item);

#if 0				/* these aren't ready yet */
extern
void
List_Insert_Item(List_Type List, List_Item_Type Item);

extern
void
List_Prepend_List(List_Type From_List, List_Type To_List);

extern
void
List_Append_List(List_Type From_List, List_Type To_List);
#endif

/* ----------------------------------------------------------------------------------------------------------------------------- */

extern
List_Item_Type
List_Replace_Item_At_Position(List_Type List, List_Item_Type Item, List_Position_Type Position);

extern
List_Item_Type
List_Replace_Item_At_Head(List_Type List, List_Item_Type Item);

extern
List_Item_Type
List_Replace_Item_At_Tail(List_Type List, List_Item_Type Item);

extern
List_Item_Type
List_Replace_Item_At_Cursor(List_Type List, List_Item_Type Item);

/* ----------------------------------------------------------------------------------------------------------------------------- */

extern
List_Item_Type
List_Remove_Item_At_Position(List_Type List, List_Position_Type Position);

extern
List_Item_Type
List_Remove_Item_At_Head(List_Type List);

extern
List_Item_Type
List_Remove_Item_At_Tail(List_Type List);

extern
List_Item_Type
List_Remove_Item_At_Cursor(List_Type List);

extern
void
List_Clear(List_Type List);

/* ----------------------------------------------------------------------------------------------------------------------------- */

extern
List_Item_Type
List_Item_At_Position(List_Type List, List_Position_Type Position);

extern
List_Item_Type
List_Item_At_Head(List_Type List);

extern
List_Item_Type
List_Item_At_Tail(List_Type List);

extern
List_Item_Type
List_Item_At_Cursor(List_Type List);

/* ----------------------------------------------------------------------------------------------------------------------------- */

extern
void
List_Reset_Cursor(List_Type List);

extern
List_Item_Type
List_Move_Cursor_To_Position(List_Type List, List_Position_Type Position);

extern
List_Item_Type
List_Move_Cursor_To_Head(List_Type List);

extern
List_Item_Type
List_Move_Cursor_To_Tail(List_Type List);

extern
List_Item_Type
List_Move_Cursor_Forward(List_Type List);

extern
List_Item_Type
List_Move_Cursor_Backward(List_Type List);

/* ----------------------------------------------------------------------------------------------------------------------------- */

extern
List_Type
List_Shallow_Copy(List_Type List);

extern
List_Type
List_Deep_Copy(List_Type List);

extern
List_Type
List_Duplicate(List_Type List); /* same as List_Deep_Copy */

/* ----------------------------------------------------------------------------------------------------------------------------- */

extern
List_Type
List_Read(List_Type List, String_Type Name, SF_Type SF);

extern
Boolean_Type
List_Write(List_Type List, String_Type Name, SF_Type SF);

/* ----------------------------------------------------------------------------------------------------------------------------- */

extern
Integer_Type
List_Length(List_Type List); 

extern
Integer_Type
List_Cursor_Position(List_Type List);

extern
Boolean_Type
List_Is_Empty(List_Type List);

extern
Boolean_Type
List_Cursor_At_Head(List_Type List);

extern
Boolean_Type
List_Cursor_At_Tail(List_Type List);

#if 0				/* these aren't ready yet */
extern
Boolean_Type
List_Is_Equal(List_Type List_1, List_Type List_2);
#endif

typedef Boolean_Type (*List_Iterator_Type)(List_Type          List,
					   List_Item_Type     Item,
					   List_Position_Type Position,
					   Generic_Type       User_Data);

extern
void
List_Iterate(List_Type          List,
	     List_Iterator_Type Process,
	     Generic_Type       User_Data);

#if 0
	     Boolean_Type (*Process)(List_Type          List,
				     List_Item_Type     Item,
				     List_Position_Type Position,
				     Generic_Type       User_Data),
extern
void
List_Iterate_In_Reverse();
#endif

#endif /* LIST_H */
