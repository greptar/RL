SCCS = "@(#) Makefile (1.4), Last modified: 16:50:04 05 Oct 1995"

SRCS	=	Array.c Bag.c Boolean.c Character.c Double.c Float.c Integer.c	\
		IO.c List.c Network.c Queue.c Stack.c SF.c String.c	\
		TS.c Tree.c

HDRS	=	Array.h Bag.h Boolean.h Character.h Double.h Float.h Integer.h	\
		IO.h List.h Network.h Queue.h Stack.h SF.h String.h	\
		TS.h Tree.h Common_Types.h Common_Delegates.h

DEPS	=	$(addprefix .,$(SRCS:.c=.d))

RL all Tests:	.configuration $(SRCS) $(HDRS) Makefile.Auxiliary Makefile
	@$(MAKE) --no-print-directory -f Makefile.Auxiliary "CC=${CC}" "SRCS=${SRCS}" "DEPS=${DEPS}" $@

clean:
	$(RM) .configuration .*.d *.o libRL.a core gmon.out

.configuration:	Configure
	$(SHELL) Configure > .configuration

.%.d:	%.c .configuration
	$(SHELL) -ec '$(CC) $(DFLAGS) $< | sed '\''s/$*.o/& $@/g'\'' > $@'

-include .configuration
