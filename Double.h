/* SCCS: @(#) Double.h (1.1), Last modified: 15:34:01 23 Oct 1995 */
#if !defined(Double_H)
#define Double_H

#include <RL/SF.h>

extern
Double_Type *
Double_Allocate(void);

extern
void
Double_Deallocate(Double_Type *Double);

/* ------------------------------------------------------------------------ */

extern
Double_Type *
Double_Read(Double_Type *Double, String_Type Name, SF_Type Save_File);

extern
Boolean_Type
Double_Write(Double_Type *Double, String_Type Name, SF_Type Save_File);

/* ------------------------------------------------------------------------ */

Double_1D_Type
Double_1D_Allocate(Integer_Type Length);
/*
   Allocates a 1D array of size [Length] (zero based).
*/

/*
   Add: Double_1D_Reallocate()
*/

void
Double_1D_Deallocate(Double_1D_Type Array);

/* ------------------------------------------------------------------------ */

Double_2D_Type
Double_2D_Allocate(Integer_Type Length_1, Integer_Type Length_2);
/*
   Allocates a 2D array of size [Length_1][Length_2] (zero based).
*/

/*
   Add: Double_2D_Reallocate()
*/

void
Double_2D_Deallocate(Double_2D_Type Array);

/* ------------------------------------------------------------------------ */

Double_3D_Type
Double_3D_Allocate(Integer_Type Length_1, Integer_Type Length_2, Integer_Type Length_3);
/*
   Allocates a 3D array of size [Length_1][Length_2][Length_3] (zero based).
*/

/*
   Add: Double_3D_Reallocate()
*/

void
Double_3D_Deallocate(Double_3D_Type Array);


#endif /* Double_H */
