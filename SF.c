#if defined(__GNUC__)
#define SCCS SCCS __attribute__ ((__unused__))
#endif

static char *SCCS = "@(#) SF.c (1.2), Last modified: 16:50:01 05 Oct 1995";

/* ------------------------------------------------------------------------- */

#if defined(NeXT)
#include <stdlib.h>		/* where malloc() is defined on the NeXT */
#else
#include <malloc.h>		/* where malloc() is defined elsewhere   */
#endif

#include <ctype.h>
#include <setjmp.h>
#include <string.h>
#include <stdio.h>

#include <RL/List.h>
#include <RL/SF.h>
#include <RL/String.h>

/* ------------------------------------------------------------------------ */

/*
   A Save File is a Collection of Name/Value pairs.  A Name is a simple
   string (such as a variable name).  A Value is a Scalar *or* a Collection
   of Scalars *or* a Collection of Name/Value pairs (which makes the save file
   recursive).

   A Collection is a generic container used to store data (such as records).
   It is up to the user to provide structure to the collection.
*/
   
/* ------------------------------------------------------------------------ */

typedef enum
{
  SF_Named_Collection,
  SF_Simple_Collection,
  SF_Compound_Collection
} SF_Collection_Type;

/*
   SF_Named_Collection    => Collection of Name/Value Pairs
   SF_Simple_Collection   => Collection of Scalars
   SF_Compound_Collection => Collection of Collections
*/

typedef enum
{
  Character_Value,
  Collection_Value,
  Comment_Value,
  Enumeration_Value,
  Float_Value,
  Integer_Value,
  String_Value
} Value_Type;

/*
   Each entry in the save file has a variable description.
*/

typedef struct _Variable_Description_Type
{
  String_Type Name;
  Value_Type  Type_Of_Value;

  union
  {
    Character_Type Character;
    String_Type    String;
    Integer_Type   Integer;
    Float_Type     Float;
    SF_Type        Collection;
  } Value;
} _Variable_Description_Type;

typedef struct _Variable_Description_Type *Variable_Description_Type;


/*
   Each "level" may only be one of the three allowed types.  The top-level
   save file is of type SF_Named_Collection.  For the save file:

     Name = "John";
     Values = 
     {
       3,
       4
     };
     Employee =
     {
       Name = "John";
       Rate = 11.50;
     };

   You end up with something like:
 
   SF => (Top Level -- Level 0)
     Collection_Type => SF_Named_Collection
     Collection      => (List of Variable_Description_Type)
     {
       {
         Name          => "Name"
         Type_Of_Value => String_Value
         Value.String  => "John"
       }
       {
         Name             => "Values"
         Type_Of_Value    => Collection_Value
         Value.Collection => (Save File -- Level 1)
           Collection_Type => SF_Simple_Collection
           Collection      => (List of Variable_Description_Type)
           {
             {
               Name                => ""
	       Type_Of_Value       => Integer_Value
	       Value.Integer_Value => 3
             }
	     {
               Name                => ""
	       Type_Of_Value       => Integer_Value
	       Value.Integer_Value => 4
	     }
           }
       }
       {
         Name             => "Employee"
         Type_Of_Value    => Collection_Value
         Value.Collection => (Save File -- Level 1)
           Collection_Type => SF_Named_Collection
           Collection      => (List of Variable_Description_Type)
           {
             {
               Name               => "Name"
	       Type_Of_Value      => String_Value
	       Value.String_Value => "John"
             }
	     {
               Name               => "Rate"
	       Type_Of_Value      => Float_Value
	       Value.Float_Value  => 11.5
	     }
           }
       }
     }
*/

typedef struct _SF_Type
{
  SF_Collection_Type Collection_Type;
  List_Type          Collection; /* A Collection of Variable_Description_Types */
} _SF_Type;




typedef struct _SF_Output_Information_Type
{
  Integer_Type  Indentation_Level;
  FILE         *Output_File;
} SF_Output_Information_Type;

/* ------------------------------------------------------------------------ */

/*
   VD_Deallocate is the deallocate delegate for the list
*/

static
void
VD_Deallocate(Variable_Description_Type VD)
{
  free(VD->Name);

  if (VD->Type_Of_Value == Collection_Value)
  {
    SF_Deallocate(VD->Value.Collection);
  }
  else if (VD->Type_Of_Value == Comment_Value ||
	   VD->Type_Of_Value == Enumeration_Value ||
	   VD->Type_Of_Value == String_Value)
  {
      free(VD->Value.String);
  }

  free(VD);
}

SF_Type
SF_Allocate(void)
{
  SF_Type SF = (SF_Type) malloc(sizeof(_SF_Type));

  SF->Collection_Type = SF_Named_Collection;
  SF->Collection      = List_Allocate(Deallocate_Delegate(VD_Deallocate), NULL);

  return (SF);
}

/* ------------------------------------------------------------------------ */

/*
   To Deallocate ...

   Loop over the SF->Collection, freeing each item
   If the item contains a collection, it must be looped over
*/

void
SF_Deallocate(SF_Type SF)
{
  List_Deallocate(SF->Collection);

  free(SF);
}

/* ------------------------------------------------------------------------ */

typedef enum
{
  Character_Token   =    1,	/* 'x'     */
  Comma_Token       =    2,	/* ,       */
  EOF_Token         =    4,	/* EOF     */
  Enumeration_Token =    8,	/* @abc    */
  Equal_Token       =   16,	/* =       */
  Float_Token       =   32,	/* xxx.xxx */
  Integer_Token     =   64,	/* xxx     */
  LBrace_Token      =  128,	/* {       */
  RBrace_Token      =  256,	/* }       */
  Semicolon_Token   =  512,	/* ;       */
  String_Token      = 1024,	/* "xxx"   */
  Variable_Token    = 2048,	/* abc     */
  Error_Token
} Token_Type;

static Token_Type     Current_Token;
static String_Type    Input_Filename;
static jmp_buf        Jump_Buffer;
static Character_Type Line_Buffer[2048];
static Integer_Type   Line_Number;
static Token_Type     Scalar_Token = Character_Token | Enumeration_Token | Float_Token | Integer_Token | String_Token;

static
void
Skip_Whitespace(FILE *Input_File)
{
  Integer_Type C;

  while ((C = fgetc(Input_File)) != EOF)
  {
    if (C == '\n')		/* end of line */
    {
      Line_Number++;
    }
    else if (C == '#')		/* comment */
    {
      while ((C = fgetc(Input_File)) != '\n');
      Line_Number++;
    }
    else if (C != ' ' && C != '\t')
    {
      break;
    }
  }

  ungetc(C, Input_File);
}

static
String_Type
Read_Variable_Name(FILE *Input_File)
{
  Integer_Type C;
  Integer_Type I = 0;

  while ((C = fgetc(Input_File)) != EOF)
  {
    if (isalpha(C) || isdigit(C) || C == '_' || C == '.')
      Line_Buffer[I++] = C;
    else
      break;
  }
  
  Line_Buffer[I] = '\0'; ungetc(C, Input_File);

  return (Line_Buffer);
}

static
String_Type
Read_Number(FILE *Input_File)
{
  Integer_Type C;
  Integer_Type I = 0;

  Line_Buffer[I++] = C = fgetc(Input_File);

  if (C == '+' || C == '-' || isdigit(C))
  {
    while ((C = fgetc(Input_File)) != EOF)
    {
      if (isdigit(C))
	Line_Buffer[I++] = C;
      else
	break;
    }
  }

  if (C == '.')			/* float number */
  {
    Line_Buffer[I++] = C;

    while ((C = fgetc(Input_File)) != EOF)
    {
      if (isdigit(C))
	Line_Buffer[I++] = C;
      else
	break;
    }

    if (C == 'e' || C == 'E')
    {
      Line_Buffer[I++] = 'e';

      Line_Buffer[I++] = C = fgetc(Input_File);

      if (C == '+' || C == '-' || isdigit(C))
      {
	while ((C = fgetc(Input_File)) != EOF)
	{
	  if (isdigit(C))
	    Line_Buffer[I++] = C;
	  else
	    break;
	}
      }
    }
  }

  Line_Buffer[I] = '\0'; ungetc(C, Input_File);

  return (Line_Buffer);
}

static
String_Type
Read_String(FILE *Input_File)
{
  Integer_Type C;
  Integer_Type I = 0;

  while ((C = fgetc(Input_File)) != EOF)
  {
    if (C != '"')
      Line_Buffer[I++] = C;
    else
      break;
  }

  Line_Buffer[I] = '\0';

  return (Line_Buffer);
}

static
Token_Type
Get_Token(FILE *Input_File)
{
  Integer_Type C, New_C;

  Skip_Whitespace(Input_File);

  C = fgetc(Input_File);

  if (feof(Input_File))
    return (Current_Token = EOF_Token);

  if (isalpha(C))
  {
    ungetc(C, Input_File);
    Read_Variable_Name(Input_File);

    return (Current_Token = Variable_Token);
  }
  else if (isdigit(C) || C == '-' || C == '+')
  {
    ungetc(C, Input_File);
    Read_Number(Input_File);

    if (strchr(Line_Buffer, '.'))
      return (Current_Token = Float_Token);
    else
      return (Current_Token = Integer_Token);
  }
  else
  {
    switch (C)
    {
    case '=':
      return (Current_Token = Equal_Token);
      break;
    case ';':
      return (Current_Token = Semicolon_Token);
      break;
    case ',':
      return (Current_Token = Comma_Token);
      break;
    case '{':
      return (Current_Token = LBrace_Token);
      break;
    case '}':
      return (Current_Token = RBrace_Token);
      break;
    case '@':
      Read_Variable_Name(Input_File);
      return (Current_Token = Enumeration_Token);
      break;
    case '"':
      Read_String(Input_File);
      return (Current_Token = String_Token);
      break;
    case '\'':
      New_C = fgetc(Input_File);

      if ((C = fgetc(Input_File)) != '\'')
      {
	fprintf(stderr, "Error loading %s:%d, expected terminating \' on character constant\n", Input_Filename, Line_Number);
	ungetc(C, Input_File);
      }

      Line_Buffer[0] = New_C; Line_Buffer[1] = '\0';
      return (Current_Token = Character_Token);

      break;
      
    default:
      fprintf(stderr, "Error loading %s:%d, unexpected character '%c' in input stream\n", Input_Filename, Line_Number, C);
      return (Current_Token = Error_Token);
    }
  }
}

static
Boolean_Type
Expect_Token(Token_Type Token_Set)
{
  Integer_Type I;
  Boolean_Type First = True;

  if (Current_Token & Token_Set)
    return (True);

  fprintf(stderr, "Error loading %s:%d, expected to find ", Input_Filename, Line_Number);

  for (I = 1; I < Error_Token; I <<= 1)
  {
    if (I & Token_Set)
    {
      if (!First)
      {
	fprintf(stderr, ", "); First = False;
      }

      switch (I)
      {
      case Character_Token:
	fprintf(stderr, "'Character'"); break;
      case Comma_Token:
	fprintf(stderr, "','"); break;
      case EOF_Token:
	fprintf(stderr, "'End-of-File'"); break;
      case Enumeration_Token:
	fprintf(stderr, "'@Enumeration'"); break;
      case Equal_Token:
	fprintf(stderr, "'='"); break;
      case Float_Token:
	fprintf(stderr, "'Float'"); break;
      case Integer_Token:
	fprintf(stderr, "'Integer'"); break;
      case LBrace_Token:
	fprintf(stderr, "'{'"); break;
      case RBrace_Token:
	fprintf(stderr, "'}'"); break;
      case Semicolon_Token:
	fprintf(stderr, "';'"); break;
      case String_Token:
	fprintf(stderr, "'String'"); break;
      case Variable_Token:
	fprintf(stderr, "'Variable'"); break;
      default:
	fprintf(stderr, "'Unknown!!!'"); break;
      }
    }
  }

  fprintf(stderr, " instead of ");

  switch (Current_Token)
  {
  case Character_Token:
    fprintf(stderr, "'%c'", Line_Buffer[0]); break;
  case Comma_Token:
    fprintf(stderr, "','"); break;
  case EOF_Token:
    fprintf(stderr, "'End-of-File'"); break;
  case Enumeration_Token:
    fprintf(stderr, "'@%s'", Line_Buffer); break;
  case Equal_Token:
    fprintf(stderr, "'='"); break;
  case Float_Token:
    fprintf(stderr, "'%s'", Line_Buffer); break;
  case Integer_Token:
    fprintf(stderr, "'%s'", Line_Buffer); break;
  case LBrace_Token:
    fprintf(stderr, "'{'"); break;
  case RBrace_Token:
    fprintf(stderr, "'}'"); break;
  case Semicolon_Token:
    fprintf(stderr, "';'"); break;
  case String_Token:
    fprintf(stderr, "'%s'", Line_Buffer); break;
  case Variable_Token:
    fprintf(stderr, "'%s'", Line_Buffer); break;
  default:
    fprintf(stderr, "'Unknown!!!'"); break;
  }

  fprintf(stderr, "\n");

  longjmp(Jump_Buffer, 1);

  return (False);
}

void
Read_Collection(SF_Type SF, FILE *Input_File, Integer_Type Level)
{
  extern double atof(String_Type);
  extern int    atoi(String_Type);

  if (Level == 0)		/* level 0 is inherently a collection of name/value pairs */
  {
    Get_Token(Input_File); Expect_Token(Variable_Token);
  }

  do
  {
    if (Level == 0 && Current_Token == EOF_Token)
      break;

    Expect_Token(Variable_Token | Scalar_Token | LBrace_Token);

    if (Current_Token == Variable_Token)
    {
      Character_Type Name[1024];

      do
      {
	strcpy(Name, Line_Buffer);
        Get_Token(Input_File); Expect_Token(Equal_Token);
        Get_Token(Input_File); Expect_Token(Scalar_Token | LBrace_Token);

        if (Current_Token & Scalar_Token)
	{
	  switch (Current_Token)
	  {
	  case Character_Token:
	    SF_Put_Character(SF, Name, Line_Buffer[0]); break;
	  case Enumeration_Token:
	    SF_Put_Enumeration(SF, Name, Line_Buffer); break;
	  case Float_Token:
	    SF_Put_Float(SF, Name, atof(Line_Buffer)); break;
	  case Integer_Token:
	    SF_Put_Integer(SF, Name, atoi(Line_Buffer)); break;
	  case String_Token:
	    SF_Put_String(SF, Name, Line_Buffer); break;
	  default:
	    break;
	  }
	}
        else if (Current_Token == LBrace_Token)
	{
	  SF_Type New_SF;

	  New_SF = SF_Begin_Collection(SF, Name);
	  Get_Token(Input_File);
	  Read_Collection(New_SF, Input_File, Level + 1);
	  SF_End_Collection(New_SF);
	}

        Get_Token(Input_File); Expect_Token(Semicolon_Token); Get_Token(Input_File);

	if (Level == 0 && Current_Token == EOF_Token)
	  break;

        Expect_Token(Variable_Token | RBrace_Token);
      } while (Current_Token != RBrace_Token);
    }
    else if (Current_Token & Scalar_Token)
    {
      do
      {
	switch (Current_Token)
	{
	case Character_Token:
	  SF_Put_Character(SF, "", Line_Buffer[0]); break;
	case Enumeration_Token:
	  SF_Put_Enumeration(SF, "", Line_Buffer); break;
	case Float_Token:
	  SF_Put_Float(SF, "", atof(Line_Buffer)); break;
	case Integer_Token:
	  SF_Put_Integer(SF, "", atoi(Line_Buffer)); break;
	case String_Token:
	  SF_Put_String(SF, "", Line_Buffer); break;
	default:
	  break;
	}

        Get_Token(Input_File); Expect_Token(Comma_Token | RBrace_Token);

	if (Current_Token == Comma_Token)
	  Get_Token(Input_File);
      } while (Current_Token != RBrace_Token);
    }
    else if (Current_Token == LBrace_Token)
    {
      do
      {
	SF_Type New_SF;

	New_SF = SF_Begin_Collection(SF, "");
	Get_Token(Input_File);
	Read_Collection(New_SF, Input_File, Level + 1);
	SF_End_Collection(New_SF);

	Get_Token(Input_File); Expect_Token(Comma_Token | RBrace_Token);

        if (Current_Token == Comma_Token)
	{
	  Get_Token(Input_File); Expect_Token(LBrace_Token);
	}
      } while (Current_Token != RBrace_Token);
    }
  } while (Current_Token != RBrace_Token);
}

Boolean_Type
SF_Load(SF_Type Save_File, String_Type Filename)
{
  FILE        *Input_File;                          /* Produces a warning using GCC  */
  Integer_Type Length = strlen(Filename);
  String_Type  Load_Filename = NULL;                /* Produces a warning using GCC  */

  if (Length > 3 && strcmp(&Filename[Length - 3], ".sf") == 0)
  {
    if ((Input_File = fopen(Filename, "r")) == NULL)
      return (False);

    Input_Filename = Filename;
  }
  else
  {
    Load_Filename = strcat(strcpy(String_Allocate(Length + 4), Filename), ".sf");
    
    if ((Input_File = fopen(Load_Filename, "r")) == NULL)
      return (False);

    Input_Filename = Load_Filename;
  }

  Line_Number = 1;

  if (setjmp(Jump_Buffer))
  {
    if (Load_Filename)
      String_Deallocate(Load_Filename);

    fclose(Input_File); SF_Deallocate(Save_File);

    return (False);
  }
    
  Read_Collection(Save_File, Input_File, 0);

  if (Load_Filename)
    String_Deallocate(Load_Filename);

  fclose(Input_File);

  return (True);
}

/* ------------------------------------------------------------------------ */

static
void
Indent_Line(FILE *Output_File, Integer_Type Indentation_Level)
{
  Integer_Type I;

  for (I = 0; I < Indentation_Level; I++)
    fputc(' ',  Output_File);
}

static
Boolean_Type
Save_Iterator(List_Type Collection, List_Item_Type Item, List_Position_Type Position, Generic_Type User_Data)
{
  SF_Output_Information_Type Output_Information = * (SF_Output_Information_Type *) User_Data;
  Variable_Description_Type  VD                 = (Variable_Description_Type) Item;

  if (Output_Information.Indentation_Level == 0)
    fputc('\n', Output_Information.Output_File);
  else
    Indent_Line(Output_Information.Output_File, Output_Information.Indentation_Level);
  
  if (VD->Name[0] == '\0')	/* Nameless entity */
  {
    switch (VD->Type_Of_Value)
    {
    case Character_Value:	/* Need to check for special cases */
      fprintf(Output_Information.Output_File, "'%c'%s\n",
	      VD->Value.Character,
	      Position == List_Length(Collection) ? "" : ",");
      break;

    case Collection_Value:
      fprintf(Output_Information.Output_File, "{\n");

      Output_Information.Indentation_Level += 2;
      List_Iterate(VD->Value.Collection->Collection, Save_Iterator, &Output_Information);
      Output_Information.Indentation_Level -= 2;

      Indent_Line(Output_Information.Output_File, Output_Information.Indentation_Level);

      fprintf(Output_Information.Output_File, "}%s\n",
	      Position == List_Length(Collection) ? "" : ",");

      break;

    case Enumeration_Value:
      fprintf(Output_Information.Output_File, "@%s%s\n",
	      VD->Value.String,
	      Position == List_Length(Collection) ? "" : ",");
      break;

    case Float_Value:
      fprintf(Output_Information.Output_File, "%f%s\n",
	      VD->Value.Float,
	      Position == List_Length(Collection) ? "" : ",");
      break;

    case Integer_Value:
      fprintf(Output_Information.Output_File, "%d%s\n",
	      VD->Value.Integer,
	      Position == List_Length(Collection) ? "" : ",");
      break;

    case String_Value:		/* Need to check for special cases */
      fprintf(Output_Information.Output_File, "\"%s\"%s\n",
	      VD->Value.String,
	      Position == List_Length(Collection) ? "" : ",");
      break;

    default:
      fprintf(stderr, "Save File: Unknown save format\n");
    }
  }
  else				/* Named entity */
  {
    switch (VD->Type_Of_Value)
    {
    case Character_Value:	/* Need to check for special cases */
      fprintf(Output_Information.Output_File, "%s = '%c';\n", VD->Name, VD->Value.Character);
      break;

    case Collection_Value:
      fprintf(Output_Information.Output_File, "%s =\n", VD->Name);
      
      Indent_Line(Output_Information.Output_File, Output_Information.Indentation_Level);

      fprintf(Output_Information.Output_File, "{\n");

      Output_Information.Indentation_Level += 2;
      List_Iterate(VD->Value.Collection->Collection, Save_Iterator, &Output_Information);
      Output_Information.Indentation_Level -= 2;

      Indent_Line(Output_Information.Output_File, Output_Information.Indentation_Level);

      fprintf(Output_Information.Output_File, "};\n");

      break;

    case Comment_Value:
      fprintf(Output_Information.Output_File, "# %s\n", VD->Name);
      break;

    case Enumeration_Value:
      fprintf(Output_Information.Output_File, "%s = @%s;\n", VD->Name, VD->Value.String);
      break;

    case Float_Value:
      fprintf(Output_Information.Output_File, "%s = %f;\n", VD->Name, VD->Value.Float);
      break;

    case Integer_Value:
      fprintf(Output_Information.Output_File, "%s = %d;\n", VD->Name, VD->Value.Integer);
      break;

    case String_Value:		/* Need to check for special cases */
      fprintf(Output_Information.Output_File, "%s = \"%s\";\n", VD->Name, VD->Value.String);
      break;

    default:
      fprintf(stderr, "Save File: Unknown save format\n");
    }
  }

  return (True);
}

Boolean_Type
SF_Save(SF_Type Save_File, String_Type Filename)
{
  Integer_Type               Length = strlen(Filename);
  SF_Output_Information_Type Output_Information;

  if (Length > 3 && strcmp(&Filename[strlen(Filename) - 3], ".sf") == 0)
  {
    if ((Output_Information.Output_File = fopen(Filename, "w")) == NULL)
      return (False);
  }
  else
  {
    String_Type Save_Filename = strcat(strcpy(String_Allocate(strlen(Filename) + 4), Filename), ".sf");
    
    if ((Output_Information.Output_File = fopen(Save_Filename, "w")) == NULL)
      return (False);

    String_Deallocate(Save_Filename);
  }

  Output_Information.Indentation_Level = 0;

  fprintf(Output_Information.Output_File, "# Save File Version 0.6\n# Destruction Release\n");
  List_Iterate(Save_File->Collection, Save_Iterator, &Output_Information);
  fclose(Output_Information.Output_File);

  return (True);
}

/* ------------------------------------------------------------------------ */

Boolean_Type
SF_Put_Comment(SF_Type Save_File, String_Type Comment)
{
  Variable_Description_Type VD = (Variable_Description_Type) malloc(sizeof(_Variable_Description_Type));

  VD->Name          = String_Duplicate(Comment);
  VD->Type_Of_Value = Comment_Value;

  List_Append_Item(Save_File->Collection, VD);

  return (True);
}

/* ------------------------------------------------------------------------ */

Boolean_Type
SF_Put_Character(SF_Type Save_File, String_Type Name, Character_Type Value)
{
  Variable_Description_Type VD = (Variable_Description_Type) malloc(sizeof(_Variable_Description_Type));

  /* Need to test to make sure name is valid or the empty string -- if not, return false */

  VD->Name            = String_Duplicate(Name);
  VD->Type_Of_Value   = Character_Value;
  VD->Value.Character = Value;

  List_Append_Item(Save_File->Collection, VD);

  return (True);
}

/* ------------------------------------------------------------------------ */

Boolean_Type
SF_Put_String(SF_Type Save_File, String_Type Name, String_Type Value)
{
  Variable_Description_Type VD = (Variable_Description_Type) malloc(sizeof(_Variable_Description_Type));

  /* Need to test to make sure name is valid or the empty string -- if not, return false */

  VD->Name          = String_Duplicate(Name);
  VD->Type_Of_Value = String_Value;
  VD->Value.String  = String_Duplicate(Value);

  List_Append_Item(Save_File->Collection, VD);

  return (True);
}

/* ------------------------------------------------------------------------ */

Boolean_Type
SF_Put_Enumeration(SF_Type Save_File, String_Type Name, String_Type Value)
{
  Variable_Description_Type VD = (Variable_Description_Type) malloc(sizeof(_Variable_Description_Type));

  /* Need to test to make sure name is valid or the empty string -- if not, return false */

  VD->Name          = String_Duplicate(Name);
  VD->Type_Of_Value = Enumeration_Value;
  VD->Value.String  = String_Duplicate(Value);

  List_Append_Item(Save_File->Collection, VD);

  return (True);
}

/* ------------------------------------------------------------------------ */

Boolean_Type
SF_Put_Integer(SF_Type Save_File, String_Type Name, Integer_Type Value)
{
  Variable_Description_Type VD = (Variable_Description_Type) malloc(sizeof(_Variable_Description_Type));

  /* Need to test to make sure name is valid or the empty string -- if not, return false */

  VD->Name          = String_Duplicate(Name);
  VD->Type_Of_Value = Integer_Value;
  VD->Value.Integer = Value;

  List_Append_Item(Save_File->Collection, VD);

  return (True);
}

/* ------------------------------------------------------------------------ */

Boolean_Type
SF_Put_Float(SF_Type Save_File, String_Type Name, Float_Type Value)
{
  Variable_Description_Type VD = (Variable_Description_Type) malloc(sizeof(_Variable_Description_Type));

  /* Need to test to make sure name is valid or the empty string -- if not, return false */

  VD->Name          = String_Duplicate(Name);
  VD->Type_Of_Value = Float_Value;
  VD->Value.Float   = Value;

  List_Append_Item(Save_File->Collection, VD);

  return (True);
}

/* ------------------------------------------------------------------------ */

SF_Type
SF_Begin_Collection(SF_Type Save_File, String_Type Name)
{
  Variable_Description_Type VD = (Variable_Description_Type) malloc(sizeof(_Variable_Description_Type));

  /* Need to test to make sure name is valid or the empty string -- if not, return false */

  VD->Name                              = String_Duplicate(Name);
  VD->Type_Of_Value                     = Collection_Value;
  VD->Value.Collection                  = (SF_Type) malloc(sizeof(_SF_Type));
  VD->Value.Collection->Collection_Type = Collection_Value;
  VD->Value.Collection->Collection      = List_Allocate(Deallocate_Delegate(VD_Deallocate), NULL);

  List_Append_Item(Save_File->Collection, VD);

  return (VD->Value.Collection);
}

/* ------------------------------------------------------------------------ */

Boolean_Type
SF_End_Collection(SF_Type Save_File)
{
  return (False);
}

/* ------------------------------------------------------------------------ */

static
Boolean_Type
Handle_Get(SF_Type Save_File, String_Type Name, Generic_Type *Value, Value_Type Type_Of_Value)
{
  Variable_Description_Type VD;

  if (Name[0] != '\0')
  {
    List_Reset_Cursor(Save_File->Collection);

    while ((VD = List_Move_Cursor_Forward(Save_File->Collection)) != NULL)
    {
      if (strcmp(Name, VD->Name) == 0)
      {
	if (VD->Type_Of_Value == Type_Of_Value)
	{
	  switch (Type_Of_Value)
	  {
	  case Character_Value:
	    * (Character_Type *) Value = VD->Value.Character;                break;
	  case Collection_Value:
	    * (SF_Type *)        Value = VD->Value.Collection;               break;
	  case Comment_Value:
	    return (False);
	  case Enumeration_Value:
	    * (String_Type *)    Value = String_Duplicate(VD->Value.String); break;
	  case Float_Value:
	    * (Float_Type *)     Value = VD->Value.Float;                    break;
	  case Integer_Value:
	    * (Integer_Type *)   Value = VD->Value.Integer;                  break;
	  case String_Value:
	    * (String_Type *)    Value = String_Duplicate(VD->Value.String); break;
	  }

	  return (True);
	}
	else
	{
	  return (False);
	}
      }
    }

    return (False);
  }
  else
  {
    VD = List_Move_Cursor_Forward(Save_File->Collection);

    if (VD && VD->Type_Of_Value == Type_Of_Value)
    {
      switch (Type_Of_Value)
      {
      case Character_Value:
	* (Character_Type *) Value = VD->Value.Character;                break;
      case Collection_Value:
	* (SF_Type *)        Value = VD->Value.Collection;               break;
      case Comment_Value:
	return (False);
      case Enumeration_Value:
	* (String_Type *)    Value = String_Duplicate(VD->Value.String); break;
      case Float_Value:
	* (Float_Type *)     Value = VD->Value.Float;                    break;
      case Integer_Value:
	* (Integer_Type *)   Value = VD->Value.Integer;                  break;
      case String_Value:
	* (String_Type *)    Value = String_Duplicate(VD->Value.String); break;
      }

      return (True);
    }
    else
    {
      return (False);
    }
  }
}

Boolean_Type
SF_Get_Character(SF_Type Save_File, String_Type Name, Character_Type *Value)
{
  return (Handle_Get(Save_File, Name, (Generic_Type *) Value, Character_Value));
}

Boolean_Type
SF_Get_String(SF_Type Save_File, String_Type Name, String_Type *Value)
{
  return (Handle_Get(Save_File, Name, (Generic_Type *) Value, String_Value));
}

Boolean_Type
SF_Get_Enumeration(SF_Type Save_File, String_Type Name, String_Type *Value)
{
  return (Handle_Get(Save_File, Name, (Generic_Type *) Value, Enumeration_Value));
}

Boolean_Type
SF_Get_Integer(SF_Type Save_File, String_Type Name, Integer_Type *Value)
{
  return (Handle_Get(Save_File, Name, (Generic_Type *) Value, Integer_Value));
}

Boolean_Type
SF_Get_Float(SF_Type Save_File, String_Type Name, Float_Type *Value)
{
  return (Handle_Get(Save_File, Name, (Generic_Type *) Value, Float_Value));
}

Boolean_Type
SF_Get_Collection(SF_Type Save_File, String_Type Name, SF_Type *Value)
{
  return (Handle_Get(Save_File, Name, (Generic_Type *) Value, Collection_Value));
}
