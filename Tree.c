#if defined(__GNUC__)
#define SCCS SCCS __attribute__ ((__unused__))
#endif

static char *SCCS = "@(#) Tree.c (1.5), Last modified: 12:56:35 20 Oct 1995";

/* ------------------------------------------------------------------------- */

#if defined(NeXT)
#include <stdlib.h>		/* where malloc() is defined on the NeXT */
#else
#include <malloc.h>		/* where malloc() is defined elsewhere   */
#endif

#include <stdarg.h>
#include <stdio.h>

#include <RL/Tree.h>

/* ------------------------------------------------------------------------- */

#if 0

The implementation of this package is based on the Red-Black Binary Tree
described in _Introduction to Algorithms_, by Cormen, Leiserson, and Rivest.
Published by McGraw Hill.

#endif

typedef enum { Red, Black } Color_Type;

typedef struct _Node_Type *Node_Type;
typedef struct _Node_Type
{
  Node_Type      Parent;
  Node_Type      Left;
  Node_Type      Right;
  Tree_Item_Type Item;
  Color_Type     Color;
} _Node_Type;

/* ------------------------------------------------------------------------- */

typedef struct _Tree_Type
{
  Node_Type                Root;		/* Tree root node          */
  Integer_Type             Size;		/* Number of Items in Tree */
  Tree_Compare_Delegate    Compare;
  Tree_Deallocate_Delegate Deallocate;
  Tree_Duplicate_Delegate  Duplicate;
  Tree_Label_Delegate      Label;
  Tree_Read_Delegate       Read;
  Tree_Write_Delegate      Write;
  Tree_Position_Type       Position;
} _Tree_Type;

/* ------------------------------------------------------------------------- */

/*
   The Sentinel is used instead of NULL for the end of the leaf-nodes.  This
   makes the deletion routine easier.  All iterations which search the tree
   must test for encountering the Sentinel pointer instead of a NULL pointer.
*/

static _Node_Type Sentinel_Node =
{
  NULL,				/* Parent */
  NULL,				/* Left   */
  NULL,				/* Right  */
  NULL,				/* Item   */
  Black				/* Color  */
};

static const Node_Type Sentinel = &Sentinel_Node;

/* ------------------------------------------------------------------------- */

static
Node_Type
Tree_Minimum(Node_Type X)
{
  while (X->Left != Sentinel)
    X = X->Left;

  return (X);
}

#if 0
static
Node_Type
Tree_Maximum(Node_Type X)
{
  while (X->Right != Sentinel)
    X = X->Right;

  return (X);
}
#endif

static
Node_Type
Tree_Successor(Node_Type X)
{
  Node_Type Y;

  if (X->Right != Sentinel)
    return (Tree_Minimum(X->Right));

  Y = X->Parent;

  while (Y != Sentinel && X == Y->Right)
    X = Y, Y = Y->Parent;

  return (Y);
}

static
Node_Type
Tree_Find_Node(Tree_Type Tree, Tree_Label_Type Label)
{
  Integer_Type Compare_Result;
  Node_Type    X = Tree->Root;

  while (X != Sentinel && (Compare_Result =
                           Tree->Compare(Label, Tree->Label(X->Item))) != 0)
    if (Compare_Result < 0)
      X = X->Left;
    else
      X = X->Right;

  return (X == Sentinel ? NULL : X);
}

/* ------------------------------------------------------------------------- */

Tree_Type
Tree_Allocate(Delegate_Type Delegate, ...)
{
  va_list      Argument_Pointer;
  Integer_Type Count = 0;
  Tree_Type    Tree = (Tree_Type) calloc(1, sizeof(_Tree_Type));

  Tree->Root      = Sentinel;

  va_start(Argument_Pointer, Delegate);

  do
  {
    if (Count++ > 15)
    {
      fprintf(stderr,
              "Tree_Allocate: Runaway delegate list,"
              "check for missing NULL terminator\n");
      break;
    }

    if (Delegate == NULL_DELEGATE)
    {
      break;
    }
    else
    {
      switch (Delegate)
      {
	Generic_Type Dummy;

      case COMPARE_DELEGATE:
	Tree->Compare = va_arg(Argument_Pointer, Generic_Type);	   break;
      case DEALLOCATE_DELEGATE:
	Tree->Deallocate = va_arg(Argument_Pointer, Generic_Type); break;
      case DUPLICATE_DELEGATE:
	Tree->Duplicate = va_arg(Argument_Pointer, Generic_Type);  break;
      case LABEL_DELEGATE:
	Tree->Label = va_arg(Argument_Pointer, Generic_Type);      break;
      case READ_DELEGATE:
	Tree->Read = va_arg(Argument_Pointer, Generic_Type);       break;
      case WRITE_DELEGATE:
	Tree->Write = va_arg(Argument_Pointer, Generic_Type);      break;
      default:
	Dummy = va_arg(Argument_Pointer, Generic_Type);            break;
      }
    }
  } while ((Delegate =
            va_arg(Argument_Pointer, Delegate_Type)) != NULL_DELEGATE);

  va_end(Argument_Pointer);

  if (Tree->Compare && Tree->Label)
    return (Tree);

  fprintf(stderr,
          "Tree: A Tree requires a Compare Delegate AND a Label Delegate\n");

  /* If we reach this point, the user didn't pass in a Compare_Delegate and the
     Tree cannot function properly.  The Tree must be deallocated. */

  free(Tree);

  return (NULL);
}

/* ------------------------------------------------------------------------- */

void
Tree_Deallocate(Tree_Type Tree)
{
  Tree_Clear(Tree); free(Tree);
}

/* ------------------------------------------------------------------------- */

static
void
Clear_It(Tree_Type Tree, Node_Type Node)
{
  if (Node->Left != Sentinel)
    Clear_It(Tree, Node->Left);

  if (Tree->Deallocate != NULL)
    Tree->Deallocate(Node->Item);

  if (Node->Right != Sentinel)
    Clear_It(Tree, Node->Right);

  free(Node);
}

void
Tree_Clear(Tree_Type Tree)
{
  if (Tree->Root != Sentinel)
    Clear_It(Tree, Tree->Root);

  Tree->Root = Sentinel;
  Tree->Size = 0;
}

/* ------------------------------------------------------------------------- */

static
void
Left_Rotate(Tree_Type T, Node_Type X)
{
  Node_Type Y;

  Y        = X->Right;
  X->Right = Y->Left;

  if (Y->Left != Sentinel)
  {
    Y->Left->Parent = X;
  }

  Y->Parent = X->Parent;

  if (X->Parent == Sentinel)
  {
    T->Root = Y;
  }
  else if (X == X->Parent->Left)
  {
    X->Parent->Left = Y;
  }
  else
  {
    X->Parent->Right = Y;
  }

  Y->Left   = X;
  X->Parent = Y;
}

static
void
Right_Rotate(Tree_Type T, Node_Type Y)
{
  Node_Type X;

  X       = Y->Left;
  Y->Left = X->Right;

  if (X->Right != Sentinel)
  {
    X->Right->Parent = Y;
  }

  X->Parent = Y->Parent;

  if (Y->Parent == Sentinel)
  {
    T->Root = X;
  }
  else if (Y == Y->Parent->Right)
  {
    Y->Parent->Right = X;
  }
  else
  {
    Y->Parent->Left = X;
  }

  X->Right  = Y;
  Y->Parent = X;
}

static
Node_Type
Tree_Insert(Tree_Type Tree, Tree_Item_Type Item)
{
  Integer_Type Compare_Result = 0;
  Node_Type    Node           = (Node_Type) malloc(sizeof(_Node_Type));
  Node_Type    X              = Tree->Root;
  Node_Type    Y              = Sentinel;

  while (X != Sentinel)
  {
    Y = X;

    if ((Compare_Result = Tree->Compare(Tree->Label(Item),
                                        Tree->Label(X->Item))) < 0)
      X = X->Left;
    else if (Compare_Result > 0)
      X = X->Right;
    else
      return (NULL);
  }

  Node->Parent = Y;
  Node->Left   = Sentinel;
  Node->Right  = Sentinel;
  Node->Item   = Item;

  if (Y == Sentinel)
  {
    Tree->Root = Node;
  }
  else if (Compare_Result < 0)
  {
    Y->Left = Node;
  }
  else
  {
    Y->Right = Node;
  }

  Tree->Size++;

  return (Node);
}

Boolean_Type
Tree_Insert_Item(Tree_Type Tree, Tree_Item_Type Item)
{
  Node_Type X = Tree_Insert(Tree, Item);
  Node_Type Y;

  if (X == NULL)		/* Item already exists ... don't insert it. */
    return (False);

  X->Color = Red;

  while (X != Tree->Root && X->Parent->Color == Red)
  {
    if (X->Parent == X->Parent->Parent->Left)
    {
      Y = X->Parent->Parent->Right;

      if (Y != Sentinel && Y->Color == Red)
      {
	X->Parent->Color         = Black;
	Y->Color                 = Black;
	X->Parent->Parent->Color = Red;
	X                        = X->Parent->Parent;
      }
      else
      {
	if (X == X->Parent->Right)
	{
	  X = X->Parent;

	  Left_Rotate(Tree, X);
	}

	X->Parent->Color         = Black;
	X->Parent->Parent->Color = Red;

	Right_Rotate(Tree, X->Parent->Parent);
      }
    }
    else
    {
      Y = X->Parent->Parent->Left;

      if (Y != Sentinel && Y->Color == Red)
      {
	X->Parent->Color         = Black;
	Y->Color                 = Black;
	X->Parent->Parent->Color = Red;
	X                        = X->Parent->Parent;
      }
      else
      {
	if (X == X->Parent->Left)
	{
	  X = X->Parent;

	  Right_Rotate(Tree, X);
	}

	X->Parent->Color         = Black;
	X->Parent->Parent->Color = Red;

	Left_Rotate(Tree, X->Parent->Parent);
      }
    }
  }

  Tree->Root->Color = Black;

  return (True);
}

/* ------------------------------------------------------------------------- */

static
void
Tree_Fixup(Tree_Type Tree, Node_Type X)
{
  Node_Type W;

  while (X != Tree->Root && X->Color == Black)
  {
    if (X == X->Parent->Left)
    {
      W = X->Parent->Right;

      if (W->Color == Red)
      {
	W->Color         = Black;
	X->Parent->Color = Red;

	Left_Rotate(Tree, X->Parent);

	W = X->Parent->Right;
      }

      if (W->Left->Color == Black && W->Right->Color == Black)
      {
	W->Color = Red;
	X        = X->Parent;
      }
      else
      {
	if (W->Right->Color == Black)
	{
	  W->Left->Color = Black;
	  W->Color       = Red;

	  Right_Rotate(Tree, W);

	  W = X->Parent->Right;
	}

	W->Color         = X->Parent->Color;
	X->Parent->Color = Black;
	W->Right->Color  = Black;

	Left_Rotate(Tree, X->Parent);

	X = Tree->Root;
      }
    }
    else
    {
      W = X->Parent->Left;

      if (W->Color == Red)
      {
	W->Color         = Black;
	X->Parent->Color = Red;

	Right_Rotate(Tree, X->Parent);

	W = X->Parent->Left;
      }

      if (W->Right->Color == Black && W->Left->Color == Black)
      {
	W->Color = Red;
	X        = X->Parent;
      }
      else
      {
	if (W->Left->Color == Black)
	{
	  W->Right->Color = Black;
	  W->Color        = Red;

	  Left_Rotate(Tree, W);

	  W = X->Parent->Left;
	}

	W->Color         = X->Parent->Color;
	X->Parent->Color = Black;
	W->Left->Color   = Black;

	Right_Rotate(Tree, X->Parent);

	X = Tree->Root;
      }
    }
  }

  X->Color = Black;
}

Tree_Item_Type
Tree_Remove_Item(Tree_Type Tree, Tree_Label_Type Label)
{
  Tree_Item_Type Item;
  Node_Type      X, Y, Z = Tree_Find_Node(Tree, Label);

  if (Z == NULL)		/* The Item isn't in the Tree. */
    return (NULL);

  Item = Z->Item;

  if (Z->Left == Sentinel || Z->Right == Sentinel)
    Y = Z;
  else
    Y = Tree_Successor(Z);

  if (Y->Left != Sentinel)
    X = Y->Left;
  else
    X = Y->Right;

  X->Parent = Y->Parent;

  if (Y->Parent == Sentinel)
    Tree->Root = X;
  else if (Y == Y->Parent->Left)
    Y->Parent->Left = X;
  else
    Y->Parent->Right = X;

  if (Y != Z)
    Z->Item = Y->Item;

  if (Y->Color == Black)
    Tree_Fixup(Tree, X);

  Tree->Size--;

  if (Tree->Deallocate)
    Tree->Deallocate(Item), Item = NULL;
/*  else
    Item = Y->Item;*/

  free(Y);

  return (Item);
}

/* ------------------------------------------------------------------------- */

extern
Tree_Item_Type
Tree_Item_At_Root(Tree_Type Tree)
{
  return (Tree->Root->Item);
}

/* ------------------------------------------------------------------------- */

static
Boolean_Type
Tree_Duplicate_Iterator(Tree_Type Tree,
                        Tree_Item_Type Item,
                        Tree_Position_Type Position,
                        Generic_Type User_Data)
{
  Tree_Type      New_Tree = User_Data;
  Tree_Item_Type New_Item = Tree->Duplicate(Item);

  Tree_Insert_Item(New_Tree, New_Item);

  return True;
}

  
extern
Tree_Type
Tree_Duplicate(Tree_Type Tree)
{
  Tree_Type New_Tree;

  if (!Tree->Duplicate)
  {
    fprintf(stderr,
            "Tree_Duplicate called on a Tree that does not "
            "have a Duplicate_Delegate\n");
    return NULL;
  }
  
  New_Tree       = (Tree_Type)calloc(1, sizeof(_Tree_Type));
  New_Tree->Root = Sentinel;

  New_Tree->Compare    = Tree->Compare;
  New_Tree->Deallocate = Tree->Deallocate;
  New_Tree->Duplicate  = Tree->Duplicate;
  New_Tree->Label      = Tree->Label;
  New_Tree->Read       = Tree->Read;
  New_Tree->Write      = Tree->Write;

  Tree_Iterate(Tree, Tree_Duplicate_Iterator, New_Tree);
  return New_Tree;
}

/* ------------------------------------------------------------------------- */

Boolean_Type
Tree_Is_Empty(Tree_Type Tree)
{
  return (Tree->Size == 0);
}

/* ------------------------------------------------------------------------- */

Tree_Item_Type
Tree_Find_Item(Tree_Type Tree, Tree_Label_Type Label)
{
  Node_Type X = Tree_Find_Node(Tree, Label);

  if (X)
    return (X->Item);
  else
    return (NULL);
}

/* ------------------------------------------------------------------------- */

Integer_Type
Tree_Size(Tree_Type Tree)
{
  return (Tree->Size);
}

/* ------------------------------------------------------------------------- */

static
Boolean_Type
Tree_Walk(Tree_Type          Tree,
	  Node_Type          Node,
	  Tree_Iterator_Type Process,
	  Generic_Type       User_Data)
{
  Boolean_Type Continue = True;

  if (Node != Sentinel)
  {
    if (Continue)
      Continue = Tree_Walk(Tree, Node->Left, Process, User_Data);

    if (Continue)
      Continue = Process(Tree, Node->Item, Tree->Position++, User_Data);

    if (Continue)
      Continue = Tree_Walk(Tree, Node->Right, Process, User_Data);
  }

  return (Continue);
}

void
Tree_Iterate(Tree_Type          Tree,
	     Tree_Iterator_Type Process,
	     Generic_Type       User_Data)
{
  Tree->Position = 1;

  if (Tree->Root)
  {
    Tree_Walk(Tree, Tree->Root, Process, User_Data);
  }
}

/* ------------------------------------------------------------------------- */

static
Boolean_Type
PB(Tree_Type Tree,
   Tree_Item_Type Item,
   Tree_Position_Type Position,
   void *User_Data)
{
  Integer_Type *Count = User_Data;

  (*Count)++;

  return (True);
}

void
Tree_Print_Balance(Tree_Type Tree)
{
  Integer_Type Count;

  if (Tree->Root != Sentinel) /* Tree->Root->Left could be the Sentinel */
  {
    Count = 0;
    Tree_Walk(Tree, Tree->Root->Left, PB, &Count);
    printf("Left = %4d\t", Count);
  }
  
  if (Tree->Root != Sentinel) /* Tree->Root->Left could be the Sentinel */
  {
    Count = 0;
    Tree_Walk(Tree, Tree->Root->Right, PB, &Count);
    printf("Right = %4d\n", Count);
  }
}

/* ------------------------------------------------------------------------- */

Tree_Type
Tree_Read(Tree_Type Tree, String_Type Name, SF_Type SF)
{
  Generic_Type Item;

  if (Tree == NULL)
  {
    /* Can't have this. */
    fprintf(stderr,
            "Tree_Read: I cannot be sucessfully called "
            "by another Collection object's Read\n");
    fprintf(stderr,
            "           function (such as Bag_Read, "
            "List_Read, Tree_Read, etc.\n");

    return (NULL);
  }
  else
  {
    SF_Type Collection;

    if (SF_Get_Collection(SF, Name, &Collection))
    {
      while ((Item = Tree->Read(NULL, "", Collection)))
      {
	Tree_Insert_Item(Tree, Item);
      }
    }
  }

  return (Tree);
}


static
Boolean_Type
Write_Iterator(Tree_Type          Tree,
	       Tree_Item_Type     Item,
               Tree_Position_Type Position,
	       Generic_Type       User_Data)
{
  SF_Type Collection = User_Data;

  if (Tree->Write(Item, "", Collection))
    return (True);
  else
    return (False);
}

Boolean_Type
Tree_Write(Tree_Type Tree, String_Type Name, SF_Type SF)
{
  if (Tree_Size(Tree))
  {
    SF_Type Collection;

    Collection = SF_Begin_Collection(SF, Name);
    Tree_Iterate(Tree, Write_Iterator, Collection);
    SF_End_Collection(Collection);

    return (True);
  }
  else
  {
    return (False);		/* Well, not quite ... */
  }
}

static
Boolean_Type
Make_Array_Iterator(Tree_Type          Tree,
                    Tree_Item_Type     Item,
                    Tree_Position_Type Position,
                    Generic_Type       User_Data)
{
  Tree_Item_Type *Array = User_Data;
  Array[Position - 1] = Item;
  return True;
}

Tree_Item_Type *
Tree_Make_Array(Tree_Type Tree)
{
  Tree_Item_Type *New_Array =
    (Tree_Item_Type *)calloc(Tree_Size(Tree)+1, sizeof(Tree_Item_Type));

  Tree_Iterate(Tree, Make_Array_Iterator, New_Array);
  return New_Array;
}
