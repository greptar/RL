/* SCCS: @(#) Tree.h (1.4), Last modified: 12:56:25 20 Oct 1995 */

#if !defined(BINARY_TREE_H)
#define BINARY_TREE_H

/* ------------------------------------------------------------------------- */

/*
   CONCEPTS:
     A Balanced Binary Tree (Red-Black Tree) is a ... well, you know ...
     (put description here).
*/

/* ------------------------------------------------------------------------- */

#include <RL/Common_Delegates.h>
#include <RL/SF.h>

/* ------------------------------------------------------------------------- */

typedef struct _Tree_Type *Tree_Type;
typedef Generic_Type       Tree_Item_Type;
typedef Generic_Type       Tree_Label_Type;
typedef Integer_Type       Tree_Position_Type;

/* ------------------------------------------------------------------------- */

typedef Integer_Type
(*Tree_Compare_Delegate)   (Tree_Label_Type Left, Tree_Label_Type Right);

typedef void
(*Tree_Deallocate_Delegate)(Tree_Item_Type Item);

typedef Tree_Item_Type
(*Tree_Duplicate_Delegate) (Tree_Item_Type Item);

typedef Tree_Label_Type
(*Tree_Label_Delegate)     (Tree_Item_Type Item);

typedef Generic_Type
(*Tree_Read_Delegate)      (Tree_Item_Type Item,
                            String_Type Name,
                            SF_Type Save_File);

typedef Boolean_Type
(*Tree_Write_Delegate)     (Tree_Item_Type Item,
                            String_Type Name,
                            SF_Type Save_File);

extern
Tree_Type
Tree_Allocate(Delegate_Type Delegate, ...);
/*
   DESCRIPTION
     Creates and initializes a new Tree instance containing zero (0) Items.
     This routine must be called before any Tree operations are performed on
     the Tree instance.  There is no programmatic limit on the number of Tree
     instances available.

   RETURN VALUE
     The new Tree instance.  This value is a required parameter in all
     subsequent Tree instance operations.  If a Tree cannot be allocated,
     NULL is returned.

   PARAMETERS
     A NULL terminated list of Delegates.

     Compare Delegate:
       The Compare Delegate is expected to return an Integer_Type indicating
       the status of a comparison.  Specifically, it should return:
           (-) if Left Label < Right Label
	   (0) if Left Label = Right Label
	   (+) if Left Label > Right Label
       Note that the left parameter is a Label and the right parameter is an
       Item.  It is the responsibility of the Compare Delegate to dereference
       both values and make the proper comparison.

       WARNING: The Tree requires a Compare Delegate in order to function
                properly (a Tree is an ordered collection).  If a Compare
		Delegate is not defined a NULL is returned.

     Deallocate Delegate:
       The Deallocate Delegate is used to deallocate the dynamic memory of an
       Item, but not the Tree instance itself.  If the Item is a simple Item
       which does not contain pointers to additional Items, the C Library
       routine "free()" can be used.  If the Item is a compound Item which
       contains pointers to additional Items, the delegate must be a
       user-defined routine which releases all dynamic memory referenced
       within the object and then deallocate the Item itself.  The Deallocate
       Delegate *always* deallocates the Item and if the Item is compound, it
       must deallocate the Items within the Item.
*/

extern
void
Tree_Deallocate(Tree_Type Tree);
/*
   DESCRIPTION
     Deallocates a Tree instance.  All Items in the Tree are freed from memory,
     if a Deallocate Delegate exists, and removed from the Tree.  After the
     Items are removed, the Tree is deallocated from memory making further
     operations on the Tree invalid.

   RETURN VALUE
     None.

   PARAMETERS
     The Tree instance.
*/

/* ------------------------------------------------------------------------- */

extern
void
Tree_Clear(Tree_Type Tree); /* works? */
/*
   DESCRIPTION
     Removes all Items from a Tree.  All Items in the Tree are freed from
     memory (if a Deallocate Delegate exists) and removed from the Tree.  After
     the Items are removed, the Tree's Size is zero (0).  The Tree is still a
     valid entity whose state is like one newly allocated with Tree_Allocate().

   RETURN VALUE
     None.

   PARAMETERS
     The Tree instance.
*/

/* ------------------------------------------------------------------------- */

extern
Boolean_Type
Tree_Insert_Item(Tree_Type Tree, Tree_Item_Type Item); /* works */
/*
   DESCRIPTION
     Inserts an Item into the Tree.  The Item is positioned in the Tree using
     the Compare Delegate.  If the Item is already in the Tree, no insertion is
     performed (use Tree_Replace_Item to replace an existing Item).

   RETURN VALUE
     True if the Item did not exist in the Tree and was successful inserted.
     False if the Item already existed in the Tree or could not be inserted.

   PARAMETERS
     The Tree instance.

     The Label for determining the Item's position in the Tree.

     The Item to insert into the Tree.
*/

#if 0
extern
Tree_Item_Type
Tree_Replace_Item(Tree_Type Tree,
                  Tree_Label_Type Label,
                  Tree_Item_Type Item); /* doesn't work */
/*
   DESCRIPTION
     Replaces an Item in the Tree or inserts a new Item into the Tree.  The
     Item is located within the Tree using the Compare Delegate.  This function
     is a superset of Tree_Insert_Item since it handles an existing/nonexisting
     Item.

   RETURN VALUE
     If the Item exists in the Tree,
       If a Deallocate Delegate exists,
         The original Item is deallocated, replaced by the new Item, and
	 NULL is returned.
       Else
         The original Item is saved, the new Item takes its place, and the
	 original Item is returned.
     Else
       The Item is inserted into the Tree and NULL is returned.

     If a Deallocate Delegate is defined, there is no way to tell, from the
     return value, if the Item existed in the Tree.  If this information is
     needed, use Tree_Find_Item prior to replacing the Item.

   PARAMETERS
     The Tree instance.

     The Label for determining the Item's position in the Tree.

     The Item to insert into the Tree.
*/
#endif

extern
Tree_Item_Type
Tree_Remove_Item(Tree_Type Tree, Tree_Label_Type Label); /* doesn't work */
/*
   DESCRIPTION
     Removes an Item from the Tree.  The Item is located within the Tree using
     the Compare Delegate.

   RETURN VALUE
     If the Item exists in the Tree,
       If a Deallocate Delegate exists,
         The original Item is deallocated and NULL is returned.
       Else
         The original Item is removed from the Tree and returned.
     Else
       NULL is returned.

     If a Deallocate Delegate is defined, there is no way to tell, from the
     return value, if the Item existed in the Tree.  If this information is
     needed, use Tree_Find_Item prior to removing the Item.

   PARAMETERS
     The Tree instance.

     The Label for determining the Item's position in the Tree.
*/

/* ------------------------------------------------------------------------- */

extern
Tree_Item_Type
Tree_Item_At_Root(Tree_Type Tree); /* works */
/*
   DESCRIPTION
     Retrieves the Item at the Root of the Tree

   RETURN VALUE
     If the Tree is empty, NULL is returned.  Otherwise, the Item
     at the Root is returned.

   PARAMETERS
     The Tree instance.
*/

/* ------------------------------------------------------------------------- */

extern
Tree_Type
Tree_Duplicate(Tree_Type Tree); /* naw working */
/*
   DESCRIPTION
     Returns a new Tree that is a copy of the one passed in.  It
     allocates new memory and uses the duplicate delegate to copy
     the items.

   RETURN VALUE
     The new Tree.

   PARAMETERS
     A Tree instance.
*/

/* ------------------------------------------------------------------------- */

extern
Boolean_Type
Tree_Is_Empty(Tree_Type Tree);	/* works */
/*
   DESCRIPTION
     Checks if the Tree is empty.  An empty Tree is a valid data structre,
     but does not contain any Items.

   RETURN VALUE
     True if the Tree is empty, False if the Tree contains Items.

   PARAMETERS
     The Tree instance.
*/

extern
Integer_Type
Tree_Size(Tree_Type Tree);	/* works */
/*
   DESCRIPTION
     Determines the size (number of Items) of the Tree.

   RETURN VALUE
     The number of Items in the Tree.  If the Tree is empty, the return value
     is zero.

   PARAMETERS
     The Tree instance.
*/

/* ------------------------------------------------------------------------- */

extern
Tree_Item_Type
Tree_Find_Item(Tree_Type Tree, Tree_Label_Type Label);
/*
   DESCRIPTION
     Finds an Item in the Tree with a given Label.  The search is made using
     the Compare Delegate.  In order to sucessfully find the Item, the Item's
     Label must match the supplied Label as determined by the Compare Delegate.

   RETURN VALUE
     The Item, if the Item can be found using the Label.  If the Item cannot be
     found (the Compare Delegate never returns a value of zero), NULL is
     returned.

   PARAMETERS
     The Tree instance.

     The search Label (or key).
*/

/* ------------------------------------------------------------------------- */

/* enum { Tree_Preorder, Tree_Inorder, Tree_Postorder }; Do these later?  -- currently unused */
  

typedef Boolean_Type (*Tree_Iterator_Type)(Tree_Type          Tree,
					   Tree_Item_Type     Item,
                                           Tree_Position_Type Position,
					   Generic_Type       User_Data);

extern
void
Tree_Iterate(Tree_Type          Tree,
	     Tree_Iterator_Type Process,
	     Generic_Type       User_Data);
/*
   DESCRIPTION
     Iterates through the entire Tree, in ascending order (as defined by the
     Compare Delegate), calling a user-supplied Process function.

   RETURN VALUE
     None.

   PARAMETERS
     The Tree instance.

     The user-supplied Process function.  The Process function receives the
     Tree being iterated over, the current Item in the iteration, and the
     user-supplied data.  The iteration continues as long as the Process
     function returns a True value.

     User-supplied data.  This is a generic pointer to data that is passed to
     the Process function.  The Iterate procedure does not use this value,
     except to pass it to the Process function.  The Process function must
     handle this value (or ignore it).  NULL can be used if there is no
     user-supplied data.
*/


extern
void
Tree_Print_Balance(Tree_Type Tree);
/*
   Debugging function ONLY.
*/

/* ------------------------------------------------------------------------- */

extern
Tree_Type
Tree_Read(Tree_Type Tree, String_Type Name, SF_Type SF);

extern
Boolean_Type
Tree_Write(Tree_Type Tree, String_Type Name, SF_Type SF);

extern
Tree_Item_Type *
Tree_Make_Array(Tree_Type Tree);

#endif /* BINARY_TREE_H */
