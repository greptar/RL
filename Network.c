#if defined(__GNUC__)
#define SCCS SCCS __attribute__ ((__unused__))
#endif

static char *SCCS = "@(#) Network.c (1.1), Last modified: 14:53:42 13 Sep 1995";

/* ------------------------------------------------------------------------- */

/* #ifdef sgi */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#ifdef sgi
#include <bstring.h>
#endif /* sgi */
#include <sys/types.h>
#include <sys/wait.h>
#ifdef sgi
#include <sys/resource.h>
#endif /* sgi */
#include <sys/param.h>
#include <sys/socket.h>
#include <netinet/in.h>
#ifdef sun			/* sun's don't define INADDR_NONE in netinet/in.h */
#define INADDR_NONE             0xffffffff
#endif
#include <netdb.h>
#include <errno.h>
#include <arpa/inet.h>
#include <stdarg.h>
#include <signal.h>
/* #endif */

#ifdef sun
int gethostname(char *name, int namelen);
void bzero(char *b, int length);
void bcopy(char *b1, char *b2, int length);
int socket(int domain, int type, int protocol);
int bind(int s, struct sockaddr *name, int namelen);
int listen(int s, int backlog);
int accept(int s, struct sockaddr *addr, int *addrlen);
int connect(int s, struct sockaddr *name, int namelen);
#endif


#include <RL/Network.h>

/* ----------------------------------------------------------------------------------------------------------------------------- */

typedef struct sockaddr_in  Internet_Address_Type;
typedef struct hostent     *Host_Entry_Type;

/* ----------------------------------------------------------------------------------------------------------------------------- */

static void Error(String_Type Error_Message)
{
  char error_message[128];
  
  strcpy( error_message, "Network." );
  strcat( error_message, Error_Message );
  
  perror( error_message );
}


#ifdef BSD
static void Handle_SIGCLD()
{
  pid_t        pid;
  int         *status;

  while ((pid = wait3(status, WNOHANG, (struct rusage *) 0)) > 0);
}
#endif


/* ----------------------------------------------------------------------------------------------------------------------------- */

IO_Descriptor_Type
Network_Allocate_Server_Port(Integer_Type Port_Number)
{
  IO_Descriptor_Type     Server_Base;
  Internet_Address_Type  Internet_Address;
  Host_Entry_Type        Host_Entry;
  Character_Type         Local_Hostname[MAXHOSTNAMELEN];

  gethostname(Local_Hostname, MAXHOSTNAMELEN);

  if ((Host_Entry = gethostbyname(Local_Hostname)) == NULL)
  {
    Error("Network_Allocate_Server_Base(Get Host By Name)");
    return (-1);
  }

  bzero((char *)&Internet_Address, sizeof(Internet_Address));

  Internet_Address.sin_addr.s_addr = htonl(INADDR_ANY);
  Internet_Address.sin_family      = Host_Entry->h_addrtype; /* AF_INET */
  Internet_Address.sin_port        = htons(Port_Number);

  bcopy((char *)Host_Entry->h_addr,
        (char *)&Internet_Address.sin_addr,
                Host_Entry->h_length );

  Server_Base = socket(Host_Entry->h_addrtype, SOCK_STREAM, 0);

  if (Server_Base < 0)
  {
    Error("Network_Allocate_Server_Base(Socket)");
    return (-1);
  }

  if (bind(Server_Base,
           (struct sockaddr *)&Internet_Address,
           sizeof(Internet_Address)) < 0)
  {
    Error("Network_Allocate_Server_Base(Bind)");
    return (-1);
  }

  if (listen(Server_Base, SOMAXCONN) < 0)
  {
    Error("Network_Allocate_Server_Base(Listen)");
    return (-1);
  }

  return (Server_Base);
}

/* ----------------------------------------------------------------------------------------------------------------------------- */

IO_Type
Network_Allocate_Server(IO_Descriptor_Type  Server_Base,
			Network_Server_Type Type_Of_Server)
{
  Integer_Type          Child_PID;
  Internet_Address_Type Internet_Address;
  int                   Size_of_Internet_Address = sizeof(Internet_Address);
  IO_Type		New_Conduit;
  
#ifdef BSD
  signal(SIGCLD, Handle_SIGCLD);
#else
  signal(SIGCLD, SIG_IGN);
#endif

  if (Server_Base < 0)
  {
    fprintf(stderr, "Server Base not open\n");
    exit(1);
  }
  

  if (Type_Of_Server == Replicating)
  {
    for (;;)
    {
      Integer_Type tmp_Descriptor;
      FILE        *tmp_Stream;
    
      tmp_Descriptor = accept(Server_Base,
			      (struct sockaddr *)&Internet_Address,
			      &Size_of_Internet_Address );
    
      /* Need to check for interrupts here - pg 525 */
      if (tmp_Descriptor < 0)
      {
	if (errno == EINTR)	/* Interrupted system call */
	{
	  continue;
	}
	else
	{
	  Error("Network_Allocate_Server(Accept)");
	  continue;
	}
      }

      tmp_Stream = fdopen(tmp_Descriptor, "w+");

      New_Conduit = IO_Allocate(Socket, tmp_Stream);
    
      if ((Child_PID = fork()) < 0)
      {
	Error("Network_Allocate_Server(Fork)");
	IO_Close(New_Conduit);
	continue;
      }
      else if ( Child_PID == 0 )	/* We are the child */
      {
	signal(SIGCLD, SIG_DFL);
	close(Server_Base);
	return (New_Conduit);
      }

      IO_Close(New_Conduit);	/* We are the parent */
    }
  }
  else				/* Iterative */
  {
    for (;;)
    {
      Integer_Type tmp_Descriptor; /* These need changed sometime ... */
      FILE        *tmp_Stream;
    
      tmp_Descriptor = accept(Server_Base,
			      (struct sockaddr *)&Internet_Address,
			      &Size_of_Internet_Address );
    
      /* Need to check for interrupts here - pg 525 */
      if (tmp_Descriptor < 0)
      {
	if (errno == EINTR)	/* Interrupted system call */
	{
	  continue;
	}
	else
	{
	  Error("Network_Allocate_Server(Accept)");
	  continue;
	}
      }

      tmp_Stream = fdopen(tmp_Descriptor, "w+");

      New_Conduit = IO_Allocate(Socket, tmp_Stream);
    
      return (New_Conduit);
    }
  }
}

/* ----------------------------------------------------------------------------------------------------------------------------- */

IO_Type
Network_Connect_To_Server(String_Type Host, Integer_Type Port_Number)
{
  Host_Entry_Type        Host_Entry;
  Internet_Address_Type  Internet_Address;
  Integer_Type           tmp_Descriptor;
  FILE                  *tmp_Stream;
  IO_Type                New_Conduit;

  bzero((char *)&Internet_Address, sizeof(Internet_Address));

  Internet_Address.sin_family = AF_INET;
  Internet_Address.sin_port   = htons(Port_Number);

  if ((Internet_Address.sin_addr.s_addr = inet_addr(Host)) == INADDR_NONE)
  {
    if ((Host_Entry = gethostbyname(Host)) == NULL)
    {
      Error("Network_Allocate_Client(Get Host By Name)");
      return (NULL);
    }
    else
    {
      bcopy(Host_Entry->h_addr,
	    (caddr_t) &Internet_Address.sin_addr,
	    Host_Entry->h_length );
    }
  }

  if ((tmp_Descriptor = socket(AF_INET, SOCK_STREAM, 0)) < 0)
  {
    Error("Network_Allocate_Client(Socket)");
    return (NULL);
  }

  tmp_Stream = fdopen(tmp_Descriptor, "w+");
  New_Conduit = IO_Allocate(Socket, tmp_Stream);
  
  if (connect(tmp_Descriptor,
	      (struct sockaddr *)&Internet_Address,
	      sizeof(Internet_Address)) < 0)
  {
    Error("Network_Allocate_Client(Connect)");
    IO_Close(New_Conduit);
    return (NULL);
  }

  return (New_Conduit);
}
