#if defined(__GNUC__)
#define SCCS SCCS __attribute__ ((__unused__))
#endif

static char *SCCS = "@(#) List.c (1.2), Last modified: 16:50:02 05 Oct 1995";

/* ------------------------------------------------------------------------- */

#if defined(NeXT)
#include <stdlib.h>		/* where malloc() is defined on the NeXT */
#else
#include <malloc.h>		/* where malloc() is defined elsewhere   */
#endif

#include <stdarg.h>
#include <stdio.h>

#include <RL/List.h>

/* ----------------------------------------------------------------------------------------------------------------------------- */

typedef struct _Node_Type *Node_Type;


typedef struct _Node_Type
{
  List_Item_Type Item;
  Node_Type      Prev;
  Node_Type      Next;
} _Node_Type;


typedef struct _List_Type
{
  Node_Type                Head;
  Node_Type                Tail;
  Node_Type                Cursor;
  Integer_Type             Cursor_Position;
  Integer_Type             Length;
  List_Deallocate_Delegate Deallocate;
  List_Duplicate_Delegate  Duplicate;
  List_Read_Delegate       Read;
  List_Write_Delegate      Write;
} _List_Type;

/* ----------------------------------------------------------------------------------------------------------------------------- */

static
Node_Type
List_Allocate_Node(List_Item_Type Item)
{
  Node_Type node;

  node  = (Node_Type) malloc(sizeof(_Node_Type));

  node->Item = Item;
  node->Prev = NULL;
  node->Next = NULL;
  
  return (node);
}


/* If position does not exist, NULL is returned */
static
Node_Type
List_Goto_Position(List_Type List, List_Position_Type Position)
{
  Node_Type    Current = List->Head;
  Integer_Type I;
  
  /* For large lists, it would be faster to use the curor position
     if it is closer to the desired position. */

  for (I = 1; I < Position; I++)
  {
    if (Current == NULL)
      return (NULL);

    Current = Current->Next;
  }

  return (Current);
}


static
void
List_Free_Node(List_Type List, Node_Type Node)
{
  if (Node != NULL)
  {
    if (List->Head == Node)
      List->Head = Node->Next;

    if (List->Tail == Node)
      List->Tail = Node->Prev;  

    if (Node->Prev != NULL)
      Node->Prev->Next = Node->Next;

    if (Node->Next != NULL)
      Node->Next->Prev = Node->Prev;

    if (List->Deallocate)
      List->Deallocate(Node->Item);

    List->Length--;

    free(Node);
  }
}

/* ----------------------------------------------------------------------------------------------------------------------------- */

List_Type
List_Allocate(Delegate_Type Delegate, ...)
{
  va_list      Argument_Pointer;
  Integer_Type Count = 0;
  List_Type    List = (List_Type) calloc(1, sizeof(_List_Type));

  va_start(Argument_Pointer, Delegate);

  do
  {
    if (Count++ > 15)
    {
      fprintf(stderr, "List_Allocate: Runaway delegate list, check for missing NULL terminator\n");
      break;
    }

    if (Delegate == NULL_DELEGATE)
    {
      break;
    }
    else
    {
      switch (Delegate)
      {
	Generic_Type Dummy;

      case DEALLOCATE_DELEGATE:
	List->Deallocate = va_arg(Argument_Pointer, Generic_Type); break;
      case DUPLICATE_DELEGATE:
	List->Duplicate = va_arg(Argument_Pointer, Generic_Type);  break;
      case READ_DELEGATE:
	List->Read = va_arg(Argument_Pointer, Generic_Type);       break;
      case WRITE_DELEGATE:
	List->Write = va_arg(Argument_Pointer, Generic_Type);      break;
      default:
	Dummy = va_arg(Argument_Pointer, Generic_Type);            break;
      }
    }
  } while ((Delegate = va_arg(Argument_Pointer, Delegate_Type)) != NULL_DELEGATE);

  va_end(Argument_Pointer);

  return (List);
}


void
List_Deallocate(List_Type List)
{
  List_Clear(List); free(List);
}

/* ----------------------------------------------------------------------------------------------------------------------------- */

void
List_Prepend_Item(List_Type List, List_Item_Type Item)
{
  Node_Type Node = List_Allocate_Node(Item);

  /* Link node into place */

  if (List->Head)
  {
    Node->Next       = List->Head;
    Node->Prev       = NULL;
    List->Head->Prev = Node;
    List->Head       = Node;
    
    /* if the cursor has been set, update the cursor position
       leaving the cursor pointing to the same item. */
    if (List->Cursor)
    {
      List->Cursor_Position++;
    }
  }
  else    /* List has no items */
  {
    List->Head = Node;
    List->Tail = Node;
    Node->Next = NULL;
    Node->Prev = NULL;
  }

  List->Length++;
}


void
List_Append_Item(List_Type List, List_Item_Type Item)
{
  Node_Type Node = List_Allocate_Node(Item);

  /* Link node into place */

  if (List->Head)
  {
    Node->Next       = NULL;
    Node->Prev       = List->Tail;
    List->Tail->Next = Node;
    List->Tail       = Node;
  }
  else    /* List has no items */
  {
    List->Head = Node;
    List->Tail = Node;
    Node->Next = NULL;
    Node->Prev = NULL;
  }

  List->Length++;
}


void
List_Insert_Item_At_Position(List_Type          List,
			     List_Item_Type     Item,
			     List_Position_Type Position)
{
  if (Position <= 1)
  {
    List_Prepend_Item(List, Item);
  }
  else if (Position <= List_Length(List))
  {
    Node_Type Node     = List_Goto_Position(List, Position);
    Node_Type New_Node = List_Allocate_Node(Item);

    New_Node->Prev   = Node->Prev;
    New_Node->Next   = Node;

#if 0
    if (Node->Prev == NULL)      /* At head */
    {
      List->Head = New_Node;
    }
    else
#endif
    {
      Node->Prev->Next = New_Node;
    }

    Node->Prev = New_Node;

    List->Length++;

    if (List->Cursor && (Position <= List->Cursor_Position))
    {
      List->Cursor_Position++;
    }
  }
  else if (Position > List_Length(List))
  {
    List_Append_Item(List, Item);
  }
}


void
List_Insert_Item_At_Cursor(List_Type      List,
			   List_Item_Type Item)
{
  if (List->Cursor)		/* Cursor points to something */
  {
    Node_Type New_Node = List_Allocate_Node(Item);

    New_Node->Prev   = List->Cursor->Prev;
    New_Node->Next   = List->Cursor;

    if (List->Cursor->Prev == NULL)      /* At head */
    {
      List->Head = New_Node;
    }
    else
    {
      List->Cursor->Prev->Next = New_Node;
    }

    List->Cursor->Prev = New_Node;

    List->Length++;
    List->Cursor_Position++;
  }
}

/* ----------------------------------------------------------------------------------------------------------------------------- */

List_Item_Type
List_Replace_Item_At_Position(List_Type          List,
			      List_Item_Type     Item,
			      List_Position_Type Position)
{
  if (Position == 1)
  {
    return (List_Replace_Item_At_Head(List, Item));
  }
  else if (Position < List_Length(List))
  {
    Node_Type      Node     = List_Goto_Position(List, Position);
    List_Item_Type Old_Item = NULL;

    if (List->Deallocate)
    {
      List->Deallocate(Node->Item);
    }
    else
    {
      Old_Item = Node->Item;
    }

    Node->Item = Item;

    return (Old_Item);
  }
  else if (Position == List_Length(List))
  {    
    return (List_Replace_Item_At_Tail(List, Item));
  }
  else
  {
    return (NULL);
  }
}


List_Item_Type
List_Replace_Item_At_Head(List_Type      List,
			  List_Item_Type Item)
{
  Node_Type      Node     = List->Head;
  List_Item_Type Old_Item = NULL;

  /* This really needs to make sure that List->Head exists */

  if (List->Deallocate)
  {
    List->Deallocate(Node->Item);
  }
  else
  {
    Old_Item = Node->Item;
  }

  Node->Item = Item;

  return (Old_Item);
}


List_Item_Type
List_Replace_Item_At_Tail(List_Type      List,
			  List_Item_Type Item)
{
  Node_Type      Node     = List->Tail;
  List_Item_Type Old_Item = NULL;

  /* This really needs to make sure that List->Tail exists */

  if (List->Deallocate)
  {
    List->Deallocate(Node->Item);
  }
  else
  {
    Old_Item = Node->Item;
  }

  Node->Item = Item;

  return (Old_Item);
}


List_Item_Type
List_Replace_Item_At_Cursor(List_Type      List,
			    List_Item_Type Item)
{
  List_Item_Type Old_Item = NULL;

  if (List->Cursor)		/* Cursor is valid */
  {
    if (List->Deallocate)
    {
      List->Deallocate(List->Cursor->Item);
    }
    else
    {
      Old_Item = List->Cursor->Item;
    }

    List->Cursor->Item = Item;
  }

  return (Old_Item);
}

/* ----------------------------------------------------------------------------------------------------------------------------- */

List_Item_Type
List_Remove_Item_At_Position(List_Type          List,
			     List_Position_Type Position)
{
  List_Item_Type Item = NULL;
  Node_Type      Node = List_Goto_Position(List, Position);

  /* Node will be NULL if the Position is not in 1 .. N */
  if (Node != NULL)
  {
    if (! List->Deallocate)
    {
      Item = Node->Item;
    }    

    List_Free_Node(List, Node);
  }

  return (Item);
}


List_Item_Type
List_Remove_Item_At_Cursor(List_Type List)
{
  List_Item_Type Item = NULL;
  Node_Type      Node = List->Cursor;

  List->Cursor = List->Cursor->Next;

  if (! List->Deallocate)
  {
    Item = Node->Item;
  }

  List_Free_Node(List, Node);

  return (Item);
}


List_Item_Type
List_Remove_Item_At_Head(List_Type List)
{
  List_Item_Type Item = NULL;
  Node_Type      Node = List->Head;

  if (Node != NULL)
  {
    if (! List->Deallocate)
    {
      Item = Node->Item;
    }

    List_Free_Node(List, Node);
  }

  return (Item);
}

List_Item_Type
List_Remove_Item_At_Tail(List_Type    List)
{
  List_Item_Type    Item = NULL;
  Node_Type Node = List->Tail;

  if (Node != NULL)
  {
    if (! List->Deallocate)
    {
      Item = Node->Item;
    }

    List_Free_Node(List, Node);
  }

  return (Item);
}


void
List_Clear(List_Type List)
{
  Node_Type Iterator = List->Tail;
  Node_Type Node_To_Delete;
  
  while (Iterator != NULL)
  {
    Node_To_Delete = Iterator;
    Iterator       = Iterator->Prev;

    List_Free_Node(List, Node_To_Delete);
  }

  List->Head   = NULL;
  List->Tail   = NULL;
  List->Length = 0;
}

/* ----------------------------------------------------------------------------------------------------------------------------- */

List_Item_Type
List_Item_At_Position(List_Type List, List_Position_Type Position)
{
  if (List && List->Head && (Position >= 1 && Position <= List->Length))
  {
    Integer_Type I;
    Node_Type    Iterator = List->Head;

    for (I = 1; I < Position; I++)
    {
      if (Iterator == NULL)
	return (NULL);

      Iterator = Iterator->Next;
    }
  
    return (Iterator->Item);
  }
  else
  {
    return (NULL);
  }
}


List_Item_Type
List_Item_At_Head(List_Type List)
{
  if (List->Head)
    return (List->Head->Item);
  else
    return (NULL);
}


List_Item_Type
List_Item_At_Tail(List_Type List)
{
  if (List->Tail)
    return (List->Tail->Item);
  else
    return (NULL);
}

List_Item_Type
List_Item_At_Cursor(List_Type List)
{
  if (List->Cursor)		/* Cursor is valid */
  {
    return (List->Cursor->Item);
  }    
  else
  {
    return (NULL);
  }
}

/* ----------------------------------------------------------------------------------------------------------------------------- */

void
List_Reset_Cursor(List_Type List)
{
  List->Cursor          = NULL;
  List->Cursor_Position = 0;
}


List_Item_Type
List_Move_Cursor_To_Position(List_Type List, List_Position_Type Position)
{
  List->Cursor = List_Goto_Position(List, Position);

  if (List->Cursor)		/* Cursor is valid */
  {
    List->Cursor_Position = Position;

    return (List->Cursor->Item);
  }
  else
  {
    List->Cursor_Position = 0;

    return (NULL);
  }
}


List_Item_Type
List_Move_Cursor_To_Head(List_Type List)
{
  List->Cursor = List->Head;

  if (List->Cursor)		/* Cursor is valid */
  {
    List->Cursor_Position = 1;

    return (List->Cursor->Item);
  }
  else
  {
    List->Cursor_Position = 0;

    return (NULL);
  }
}


List_Item_Type
List_Move_Cursor_To_Tail(List_Type List)
{
  List->Cursor = List->Tail;

  if (List->Cursor)		/* Cursor is valid */
  {
    List->Cursor_Position = List_Length(List);

    return (List->Cursor->Item);
  }
  else
  {
    List->Cursor_Position = 0;

    return (NULL);
  }
}


List_Item_Type
List_Move_Cursor_Forward(List_Type List)
{
  if (List->Cursor)		/* Cursor is valid */
  {
    List->Cursor = List->Cursor->Next;

    if (List->Cursor)		/* New Cursor is valid */
    {
      List->Cursor_Position++;

      return (List->Cursor->Item);
    }
    else			/* New Cursor is invalid */
    {
      List->Cursor_Position = 0;

      return (NULL);
    }
  }
  else				/* Cursor should move to head */
  {
    return (List_Move_Cursor_To_Head(List));
  }
}


List_Item_Type
List_Move_Cursor_Backward(List_Type List)
{
  if (List->Cursor)		/* Cursor is valid */
  {
    List->Cursor = List->Cursor->Prev;

    if (List->Cursor)		/* New Cursor is valid */
    {
      List->Cursor_Position--;

      return (List->Cursor->Item);
    }
    else			/* New Cursor is invalid */
    {
      List->Cursor_Position = 0;

      return (NULL);
    }
  }
  else				/* Cursor should move to tail */
  {
    return (List_Move_Cursor_To_Tail(List));
  }
}

/* ----------------------------------------------------------------------------------------------------------------------------- */

static
Boolean_Type
Shallow_Iterator(List_Type          From_List,
		 List_Item_Type     Item,
		 List_Position_Type Position,
		 Generic_Type       To_List)
{
  List_Append_Item(To_List, Item);

  return (True);
}

List_Type
List_Shallow_Copy(List_Type List)
{
  List_Type New_List = List_Allocate(Deallocate_Delegate(List->Deallocate),
                                     Duplicate_Delegate(List->Duplicate),
                                     Read_Delegate(List->Read),
                                     Write_Delegate(List->Write),
                                     NULL);
  
  List_Iterate(List, Shallow_Iterator, New_List);

  return (New_List);
}


static
Boolean_Type
Deep_Iterator(List_Type          From_List,
	      List_Item_Type     Item,
	      List_Position_Type Position,
	      Generic_Type       To_List)
{
  List_Append_Item(To_List, From_List->Duplicate(Item));

  return (True);
}

List_Type
List_Deep_Copy(List_Type List)
{
  List_Type New_List = List_Allocate(Deallocate_Delegate(List->Deallocate),
                                     Duplicate_Delegate(List->Duplicate),
                                     Read_Delegate(List->Read),
                                     Write_Delegate(List->Write),
                                     NULL);
  
  List_Iterate(List, Deep_Iterator, New_List);

  return (New_List);
}

List_Type
List_Duplicate(List_Type List)
{
  return (List_Deep_Copy(List));
}

/* ----------------------------------------------------------------------------------------------------------------------------- */

List_Type
List_Read(List_Type List, String_Type Name, SF_Type SF)
{
  Generic_Type Item;

  if (List == NULL)
  {
    /* Can't have this. */
    fprintf(stderr, "List_Read: I cannot be sucessfully called by another Collection object's Read\n");
    fprintf(stderr, "           function (such as Bag_Read, List_Read, Tree_Read, etc.\n");

    return (NULL);
  }
  else
  {
    SF_Type Collection;

    if (SF_Get_Collection(SF, Name, &Collection))
    {
      while ((Item = List->Read(NULL, "", Collection)))
      {
	List_Append_Item(List, Item);
      }
    }
  }

  return (List);
}


static
Boolean_Type
Write_Iterator(List_Type          List,
	       List_Item_Type     Item,
	       List_Position_Type Position,
	       Generic_Type       User_Data)
{
  SF_Type Collection = User_Data;

  if (List->Write(Item, "", Collection))
    return (True);
  else
    return (False);
}

Boolean_Type
List_Write(List_Type List, String_Type Name, SF_Type SF)
{
  if (List_Length(List))
  {
    SF_Type Collection;

    Collection = SF_Begin_Collection(SF, Name);
    List_Iterate(List, Write_Iterator, Collection);
    SF_End_Collection(Collection);

    return (True);
  }
  else
  {
    return (False);		/* Well, not quite ... */
  }
}

/* ----------------------------------------------------------------------------------------------------------------------------- */

Integer_Type
List_Length(List_Type List)
{
  if (List)
  {
    return (List->Length);
  }
  else
  {
    return (0);
  }
}


Integer_Type
List_Cursor_Position(List_Type List)
{
  if (List)
  {
    return (List->Cursor_Position);
  }
  else
  {
    return (0);
  }
}


Boolean_Type
List_Is_Empty(List_Type List)
{
  if (List)
  {
    return (List->Length == 0);
  }
  else
  {
    return (True);
  }
}


Boolean_Type
List_Cursor_At_Head(List_Type List)
{
  if (List && List->Head)
  {
    return (List->Head == List->Cursor);
  }
  else
  {
    return (False);
  }
}


extern
Boolean_Type
List_Cursor_At_Tail(List_Type List)
{
  if (List && List->Tail)
  {
    return (List->Tail == List->Cursor);
  }
  else
  {
    return (False);
  }
}


void
List_Iterate(List_Type          List,
	     List_Iterator_Type Process,
	     Generic_Type       User_Data)
{
  if (List && List->Head)
  {
    Node_Type          Iterator = List->Head;
    List_Position_Type Position = 1;

    while (Iterator != NULL)
    {
      if (Process(List, Iterator->Item, Position, User_Data) == 0)
	break;

      Position++;
      Iterator = Iterator->Next;
    }
  }
}
