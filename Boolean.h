/* SCCS: @(#) Boolean.h (1.1), Last modified: 14:53:21 13 Sep 1995 */

#if !defined(BOOLEAN_H)
#define BOOLEAN_H

#include <RL/SF.h>

/*
   Someday I hope to create an Enumeration package where I can register
   name/value pairs and have Boolean use it (kind of like a subclass).
*/

/* ------------------------------------------------------------------------ */

extern
Boolean_Type *
Boolean_Allocate(void);

extern
void
Boolean_Deallocate(Boolean_Type *Boolean);

/* ------------------------------------------------------------------------ */

extern
Boolean_Type *
Boolean_Read(Boolean_Type *Boolean, String_Type Name, SF_Type SF);

extern
Boolean_Type
Boolean_Write(Boolean_Type *Boolean, String_Type Name, SF_Type SF);

/*
   If Name is an empty string, it is part of a nameless collection.
*/

#endif /* BOOLEAN_H */
