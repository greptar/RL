/* SCCS: @(#) Float.h (1.1), Last modified: 14:53:14 13 Sep 1995 */

#if !defined(FLOAT_H)
#define FLOAT_H

#include <RL/SF.h>

extern
Float_Type *
Float_Allocate(void);

extern
void
Float_Deallocate(Float_Type *Float);

/* ------------------------------------------------------------------------ */

extern
Float_Type *
Float_Read(Float_Type *Float, String_Type Name, SF_Type Save_File);

extern
Boolean_Type
Float_Write(Float_Type *Float, String_Type Name, SF_Type Save_File);

/* ------------------------------------------------------------------------ */

Float_1D_Type
Float_1D_Allocate(Integer_Type Length);
/*
   Allocates a 1D array of size [Length] (zero based).
*/

/*
   Add: Float_1D_Reallocate()
*/

void
Float_1D_Deallocate(Float_1D_Type Array);

/* ------------------------------------------------------------------------ */

Float_2D_Type
Float_2D_Allocate(Integer_Type Length_1, Integer_Type Length_2);
/*
   Allocates a 2D array of size [Length_1][Length_2] (zero based).
*/

/*
   Add: Float_2D_Reallocate()
*/

void
Float_2D_Deallocate(Float_2D_Type Array);

/* ------------------------------------------------------------------------ */

Float_3D_Type
Float_3D_Allocate(Integer_Type Length_1, Integer_Type Length_2, Integer_Type Length_3);
/*
   Allocates a 3D array of size [Length_1][Length_2][Length_3] (zero based).
*/

/*
   Add: Float_3D_Reallocate()
*/

void
Float_3D_Deallocate(Float_3D_Type Array);


#endif /* FLOAT_H */
