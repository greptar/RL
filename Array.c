#if defined(__GNUC__)
#define SCCS SCCS __attribute__ ((__unused__))
#endif

static char *SCCS = "@(#) Array.c (1.1), Last modified: 14:53:58 13 Sep 1995";

/* ------------------------------------------------------------------------- */

#if defined(NeXT)
#include <stdlib.h>		/* where malloc() is defined on the NeXT */
#else
#include <malloc.h>		/* where malloc() is defined elsewhere   */
#endif

#include <stdio.h>		/* where NULL is defined on SunOS */

#include <RL/Array.h>

/* ----------------------------------------------------------------------------------------------------------------------------- */

Array_1D_Type
Array_1D_Allocate(Integer_Type Length, Integer_Type Item_Size)
{
  Array_1D_Type Result;

  Result = (Array_1D_Type) malloc(Item_Size * Length);

  return (Result);
}

void
Array_1D_Deallocate(Array_1D_Type Array)
{
  free(Array);
}

/* ----------------------------------------------------------------------------------------------------------------------------- */

Array_2D_Type
Array_2D_Allocate(Integer_Type Length_1, Integer_Type Length_2, Integer_Type Item_Size)
{
  Integer_Type  I;
  Array_2D_Type Result;

  Result = (Array_2D_Type) malloc(sizeof(Array_1D_Type) * (Length_1 + 1));

  for (I = 0; I < Length_1; I++)
  {
    Result[I] = (Array_1D_Type) malloc(Item_Size * Length_2);
  }

  Result[Length_1] = NULL;	/* The end of the array */

  return (Result);
}

void
Array_2D_Deallocate(Array_2D_Type Array)
{
  Integer_Type I;

  for (I = 0; Array[I] != NULL; I++)
  {
    free(Array[I]);
  }

  free(Array);
}

/* ----------------------------------------------------------------------------------------------------------------------------- */

Array_3D_Type
Array_3D_Allocate(Integer_Type Length_1, Integer_Type Length_2, Integer_Type Length_3, Integer_Type Item_Size)
{
  Integer_Type  I, J;
  Array_3D_Type Result;

  Result = (Array_3D_Type) malloc(sizeof(Array_2D_Type) * (Length_1 + 1));

  for (I = 0; I < Length_1; I++)
  {
    Result[I] = (Array_2D_Type) malloc(sizeof(Array_1D_Type) * (Length_2 + 1));

    for (J = 0; J < Length_2; J++)
    {
      Result[I][J] = (Array_1D_Type) malloc(Item_Size * Length_3);
    }

    Result[I][Length_2] = NULL;
  }

  Result[Length_1] = NULL;

  return (Result);
}

void
Array_3D_Deallocate(Array_3D_Type Array)
{
  Integer_Type I, J;

  for (I = 0; Array[I] != NULL; I++)
  {
    for (J = 0; Array[I][J] != NULL; J++)
    {
      free(Array[I][J]);
    }

    free(Array[I]);
  }

  free(Array);
}

/* ----------------------------------------------------------------------------------------------------------------------------- */
