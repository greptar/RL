/* SCCS: @(#) Integer.h (1.1), Last modified: 14:53:10 13 Sep 1995 */

#if !defined(INTEGER_H)
#define INTEGER_H

#include <RL/SF.h>

extern
Integer_Type *
Integer_Allocate(void);

extern
void
Integer_Deallocate(Integer_Type *Integer);

/* ----------------------------------------------------------------------------------------------------------------------------- */

extern
Integer_Type *
Integer_Read(Integer_Type *Integer, String_Type Name, SF_Type SF);

extern
Boolean_Type
Integer_Write(Integer_Type *Integer, String_Type Name, SF_Type SF);

/*
   If Name is an empty string, it is part of a nameless collection.
*/

/* ----------------------------------------------------------------------------------------------------------------------------- */

extern
Integer_1D_Type
Integer_1D_Allocate(Integer_Type Length);
/*
   Allocates a 1D array of size [X] (zero based).
*/

/*
   Add: Integer_1D_Reallocate()
*/

extern
void
Integer_1D_Deallocate(Integer_1D_Type Array);

/* ----------------------------------------------------------------------------------------------------------------------------- */

extern
Integer_2D_Type
Integer_2D_Allocate(Integer_Type Length_1, Integer_Type Length_2);
/*
   Allocates a 2D array of size [X][Y] (zero based).
*/

/*
   Add: Integer_2D_Reallocate()
*/

extern
void
Integer_2D_Deallocate(Integer_2D_Type Array);

/* ----------------------------------------------------------------------------------------------------------------------------- */

extern
Integer_3D_Type
Integer_3D_Allocate(Integer_Type Length_1, Integer_Type Length_2, Integer_Type Length_3);
/*
   Allocates a 3D array of size [X][Y][Z] (zero based).
*/

/*
   Add: Integer_3D_Reallocate()
*/

extern
void
Integer_3D_Deallocate(Integer_3D_Type Array);

/* ----------------------------------------------------------------------------------------------------------------------------- */


extern
String_Type
Integer_To_String(Integer_Type Value);


/*
extern
Integer_Type
Integer_Compare()
Integer_Duplicate()
Integer_Free()

Integer_1D_Read()
Integer_1D_Write()
*/

#endif /* INTEGER_H */
