/* SCCS: @(#) Array.h (1.1), Last modified: 14:53:26 13 Sep 1995 */

#if !defined(ARRAY_H)
#define ARRAY_H

#include <RL/Common_Types.h>


typedef void   *Array_1D_Type;
typedef void  **Array_2D_Type;
typedef void ***Array_3D_Type;


extern
Array_1D_Type
Array_1D_Allocate(Integer_Type Length, Integer_Type Item_Size);

extern
void
Array_1D_Deallocate(Array_1D_Type Array);


extern
Array_2D_Type
Array_2D_Allocate(Integer_Type Length_1, Integer_Type Length_2, Integer_Type Item_Size);

extern
void
Array_2D_Deallocate(Array_2D_Type Array);


extern
Array_3D_Type
Array_3D_Allocate(Integer_Type Length_1, Integer_Type Length_2, Integer_Type Length_3, Integer_Type Item_Size);

extern
void
Array_3D_Deallocate(Array_3D_Type Array);


/*
   Need an:
     Array_1D_Reallocate(Array_1D_Type Array, Integer_Type Length);
     Array_2D_Reallocate();
     Array_3D_Reallocate();

     Array, like String (which is a specialized array of Characters), requires
     the address of the array to be passed for the Read functions.

     Array_1D_Read(Array_1D_Type *Array, String_Type Name, SF_Type SF),
     Array_1D_Write(Array_1D_Type Array, String_Type Name, SF_Type SF);
     Array_2D_Read(), Array_2D_Write();
     Array_3D_Read(), Array_3D_Write();

     Integer_Type Array_1D_Size(Array_1D_Type Array);
     Integer_Type Array_2D_Size(Array_2D_Type Array, Integer_Type Dimension); // 1-based
     Integer_Type Array_3D_Size(Array_3D_Type Array, Integer_Type Dimension); // 1-based
*/

#endif /* ARRAY_H */
