/* SCCS: @(#) String.h (1.3), Last modified: 14:31:13 16 Oct 1995 */

#if !defined(STRING_H)
#define STRING_H

#include <RL/SF.h>

/* ------------------------------------------------------------------------ */

extern
String_Type
String_Read(String_Type *String, String_Type Name, SF_Type SF);

extern
Boolean_Type
String_Write(String_Type String, String_Type Name, SF_Type SF);

/*
   If Name is an empty string, it is part of a nameless collection.
*/


extern
Integer_Type
String_Length(String_Type String);

extern
String_Type
String_Duplicate(String_Type String);

extern
String_Type
String_Label(String_Type String);

extern
String_Type
String_Allocate(Integer_Type Length);

extern
String_Type
String_Reallocate(String_Type String, Integer_Type Length);

extern
void
String_Deallocate(String_Type String);

extern
String_Type
String_Prepend(String_Type String_1, String_Type String_2);
/*
   Reallocates String_2 to be large enough to contain itself and String_1, then
   prepends String_1 to the beginning of String_2 and returns the new pointer to
   String_2.  Example: My_String = String_Prepend("XYZ_", My_String);  String_1
   is not altered while String_2 is altered.
*/

extern
String_Type
String_Append(String_Type String_1, String_Type String_2);
/*
   Reallocates String_1 to be large enough to contain itself and String_2, then
   appends String_2 to the end of String_1 and returns the new pointer to String_1.
   Example: My_String = String_Append(My_String, ".tiff");  String_1 is altered
   while String_2 is not altered.
*/

/* ------------------------------------------------------------------------ */

extern
Integer_Type
String_Compare(String_Type String1, String_Type String2);

/*
  strncat();
  strncmp();
  strncpy();
  strncasecmp();

  Integer_Type
  String_Compare(String_Type String1, String_Type String2);

  Integer_Type
  String_Insensitive_Compare(String_Type String1, String_Type String2);

  String_Type
  String_Duplicate(String_Type Original_String);

     char *strchr(const char *s, int c);

     char *strrchr(const char *s, int c);

     size_t strspn(const char *s1, const char *s2);

     size_t strcspn(const char *s1, const char *s2);

     char *strpbrk(const char *s1, const char *s2);

     char *strstr(const char *s1, const char *s2);

     char *strtok(char *s1, const char *s2);


		     char *index(const char *s, int c);

		     char *rindex(const char *s, int c);

DESCRIPTION
     These functions operate on null-terminated strings.  They do
     not check for overflow of any receiving string.

     Strcat appends a copy of string append to the end of string
     s. Strncat copies at most count characters.  Both return a
     pointer to the null-terminated result.

     Strcmp compares its arguments and returns an integer greater
     than, equal to, or less than 0, according as s1 is lexico-
     graphically greater than, equal to, or less than s2.
     Strncmp makes the same comparison but looks at at most count
     characters.

     Strcpy copies string from to to, stopping after the null
     character has been moved.  Strncpy copies exactly count
     characters, appending nulls if from is less than count char-
		     acters in length; the target may not be null-terminated if
     the length of from is count or more.  Both return to.

     Strchr locates the first occurrence of c in the string
     pointed to by s.  The terminating null character is not con-
     sidered to be part of the string.  Strchr returns a pointer
     to the located character, or a null pointer if the character
     does not occur in the string.

     Strrchr locates the last occurrence of c in the string
     pointed to by s.  The terminating null character is not con-
     sidered to be part of the string.  Strrchr returns a pointer
     to the located character, or a null pointer if the character
     does not occur in the string.

     Strspn computes the length of the maximum initial segment of
     the string pointed to by s1 which consists entirely of char-
     acters from the string pointed to by s2. Strspn returns the
     length of the segment.

     Strcspn computes the length of the maximum initial segment
     of the string pointed to by s1 which consists entirely of
     characters not from the string pointed to by s2. Strcspn
     returns the length of the segment.

     Strpbrk locates the first occurrence in the string pointed
     to by s1 of any character from the string pointed to by s2.
     Strpbrk returns a pointer to the character, or a null
     pointer if no character from s2 occurs in s1.

     Strstr locates the first occurrence in the string pointed to
     by s1 of the sequence of characters (excluding the terminat-
     ing null character) in the string pointed to by s2.  Strstr
     returns a pointer to the located string, or a null pointer
     if the string is not found.  If s2 points to a string with
     zero length, s1 is returned.

     A sequence of calls to strtok breaks the string pointed to
     by s1 into a sequence of tokens, each of which is delimited
     by a character from the string pointed to by s2.  The first
     call in the sequence has s1 as its first argument, and is
     followed by calls with a null pointer as their first argu-
     ment.  The separator string pointed to by s2 may be dif-
     ferent from call to call.

     The first call in the sequence searches the string pointed
     to by s1 for the first character that is not contained in
     the current separator string pointed to by s2.  If no such
     character is found, then there are no tokens in the string
     pointed to by s1 and strtok returns a null pointer.  If such
     a character is found, it is the start of the first token.

     Strtok then searches from there for a character that is con-
     tained in the current separator string.  If no such charac-
     ter is found, the current token extends to the end of the
     string pointed to by s1, and subsequent searches for a token
     will return a null pointer.  If such a character is found,
     it is overwritten by a null character, which terminates the
     current token. Strtok saves a pointer to the following char-
     acter, from which the next search for a token will start.

     Each subsequent call, with a null pointer  as the value of
     the first argument, starts searching from the saved pointer
     and behaves as described above.

     Strtok returns a pointer to the first character of a token,
     or a null pointer if there is no token.

     Strlen returns the number of non-null characters in s.

BSD ADDITIONS
     The strcasecmp and strncasecmp functions perform as strcmp
     and strncmp, but are case-insensitive (that is, they convert
     characters in each string to lowercase before comparing
     them).

     The index and rindex functions are equivalent to strchr and
     strrchr, respectively.
*/
#endif /* STRING_H */
