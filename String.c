#if defined(__GNUC__)
#define SCCS SCCS __attribute__ ((__unused__))
#endif

static char *SCCS = "@(#) String.c (1.3), Last modified: 14:30:42 16 Oct 1995";

/* ------------------------------------------------------------------------- */

#if defined(NeXT)
#include <stdlib.h>		/* where malloc() is defined on the NeXT */
#else
#include <malloc.h>		/* where malloc() is defined elsewhere   */
#endif

#include <memory.h>
#include <stdio.h>		/* where NULL is defined on SunOS */
#include <string.h>

#include <RL/String.h>

/* ------------------------------------------------------------------------ */

String_Type
String_Read(String_Type *String, String_Type Name, SF_Type SF)
{
  if (String == NULL)
  {
    String_Type New_String = NULL;

    if (SF_Get_String(SF, Name, &New_String))
      return (New_String);
  }
  else
  {
    if (*String)
      free(*String);

    if (SF_Get_String(SF, Name, String))
      return (*String);
  }

  return (NULL);
}

Boolean_Type
String_Write(String_Type String, String_Type Name, SF_Type SF)
{
  return (SF_Put_String(SF, Name, String));
}

Integer_Type
String_Length(String_Type String)	/* You should see the GNU strlen ... wow */
{
  return (strlen(String));	/* use regular strlen() */
#if 0

  String_Type S = String;

  while (*S)
    S++;

  return (S - String);
#endif
}
  
String_Type
String_Duplicate(String_Type String) /* Some systems (DEC, NeXT) don't have strdup */
{
  Integer_Type Length = String_Length(String) + 1;
  String_Type  Result = String_Allocate(Length);

  if (Result == NULL)
    return (NULL);

  memcpy(Result, String, Length);

  return (Result);
}

String_Type
String_Label(String_Type String)
{
  return String;
}

Integer_Type
String_Compare(String_Type String1, String_Type String2)
{
  return strcmp(String1, String2);
}

String_Type
String_Append(String_Type String_1, String_Type String_2)
{
  Integer_Type L1 = String_Length(String_1);
  Integer_Type L2 = String_Length(String_2);
  String_Type  NS = String_Reallocate(String_1, L1 + L2 + 1);

  memcpy(&NS[L1], String_2, L2);

  return (NS);
}

String_Type
String_Allocate(Integer_Type Length)
{
  return (malloc(sizeof(Character_Type) * Length));
}

String_Type
String_Reallocate(String_Type String, Integer_Type Length)
{
  return (realloc(String, sizeof(Character_Type) * Length));
}

void
String_Deallocate(String_Type String)
{
  free(String);
}
