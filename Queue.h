/* SCCS: @(#) Queue.h (1.1), Last modified: 14:52:55 13 Sep 1995 */

#if !defined(QUEUE_H)
#define QUEUE_H

#include <RL/Common_Delegates.h>
#include <RL/SF.h>

typedef struct _Queue_Type *Queue_Type;
typedef void               *Queue_Item_Type;


typedef Integer_Type (*Queue_Compare_Delegate)(Queue_Item_Type Item);
typedef void         (*Queue_Deallocate_Delegate)(Queue_Item_Type Item);


extern
Queue_Type
Queue_Allocate(Delegate_Type Delegate, ...);
/*
   If Compare_Delegate is defined, the queue acts as a priority queue.
*/

extern
void
Queue_Deallocate(Queue_Type Queue);

extern
void
Queue_Clear(Queue_Type Queue);

extern
void
Queue_Enqueue_Item(Queue_Type Queue, Queue_Item_Type Item);
/*
   Add the item at the tail of the queue, unless the queue is a priority queue
   which positions the item based on priority.
*/

extern
Queue_Item_Type
Queue_Dequeue_Item(Queue_Type Queue);
/*
   Removes an item from the head of the queue.
*/

extern
Queue_Item_Type
Queue_Item_At_Head(Queue_Type Queue);
/*
   Similar to Dequeue, but leaves the Item on the queue.
*/

extern
Boolean_Type
Queue_Is_Empty(Queue_Type Queue);

extern
Integer_Type
Queue_Length(Queue_Type Queue);

#endif /* QUEUE_H */
