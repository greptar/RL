/* SCCS: @(#) IO.h (1.1), Last modified: 14:53:12 13 Sep 1995 */

#ifndef IO_H
#define IO_H

#include <RL/Common_Types.h>

/*
   This is the new IO_Type.  It is used instead of using file descriptors
   or streams for IO.
*/

typedef Integer_Type     IO_Descriptor_Type;
typedef struct _IO_Type *IO_Type;

/*
   These are the different types of connections that can be managed with
   IO_Types.
*/

typedef enum
{
  Memory_Mapped,
  Socket,
  Stream
} IO_Connection_Type;

/*
   Ignore these.  These are needed only for the following #defines.
*/
extern IO_Type IO_Standard_Input(void);
extern IO_Type IO_Standard_Output(void);
extern IO_Type IO_Standard_Error(void);

/*
   These are the IO_Types to use if you want to use any of the
   IO functions in this header file on standard in, out, or error.
*/
#define IO_stdin  IO_Standard_Input()
#define IO_stdout IO_Standard_Output()
#define IO_stderr IO_Standard_Error()


extern
IO_Type
IO_Allocate(IO_Connection_Type Connection_Type, ...);
/*
   This function is called by the Allocate functions in Network.h
   and File.h.  It is used to create an IO_Type from Sockets,
   Streams, and memory mapped files.  This function is generally not
   used outside of this library, but can be used if <stdio.h> was
   used to open a file and you wish to do IO on that file with
   routines in this header file.  For example:
     FILE       *fp;
     IO_Type     Input_File;
     String_Type Line;
     
     fp = fopen("Filename.txt", "r");
     Input_File = IO_Allocate(Stream, fp);
     Line = IO_Get_Line(Input_File);
     .
     .
     .
*/


extern
Integer_Type
IO_Read(IO_Type      Conduit,
	Generic_Type Buffer,
	Integer_Type Length);
/*
   IO_Read copies into _Buffer_ up to _Length_ bytes of data from
   the IO_Type _Conduit_.  IO_Read stops reading bytes if an
   end-of-file or error condition is encountered while reading
   _Conduit_, or if _Length_ bytes have been read.
   IO_Read returns the number of bytes read.
  
   Note:  _Buffer_ must already be allocated.  If _Length_ exceeds
          the number of bytes allocated to _Buffer_, segmentation
          violations may happen.
*/


extern
Integer_Type
IO_Write(IO_Type      Conduit,
	 Generic_Type Buffer,
	 Integer_Type Length);
/*
   IO_Write appends at most _Length_ bytes of data from _Buffer_
   onto _Conduit_.  IO_Write stops appending when it has appended
   _Length_ bytes of data or if an error condition is encountered on
   _Conduit_.  IO_Write does not change the contents of _Buffer_
   IO_Write returns the number of bytes written.
  
   Note:  If _Length_ exceeds the number of bytes allocated to
          _Buffer_, segmentation violations may happen.
*/


extern
void
IO_Close(IO_Type Conduit);
/*
   IO_Close causes any buffered data waiting to be written to _Conduit_
   to be written out, and _Conduit_ to be closed.
*/


extern
void
IO_Flush(IO_Type Conduit);
/*
   IO_Flush causes any buffered data waiting to be written to _Conduit_
   to be written out
*/


extern
Integer_Type
IO_Printf(IO_Type Conduit, String_Type Format, ...);
/*
   IO_Printf places output on _Conduit_.  _Format_ and the arguments
   following _Format_ work exactly like fprintf in <stdio.h>.
   For more information, look at the man page for fprintf.
   What is returned is the number of characters written.
*/


/* Modify this later for the IO_Type parameter to be first - mrg */
extern
String_Type
IO_Gets(String_Type 	Buffer,
	Integer_Type 	Length,
	IO_Type 	Conduit);
/*
   IO_Gets reads characters from _Conduit_ into _Buffer_ until 
   Length-1 characters are read, or a new-line character is read and
   transferred to _Buffer_, or an end-of-file condition is encountered.
   The string is then terminated with a null character.  What is 
   returned is Buffer if there are no errors, or NULL if there are
   errors.
  
   Note:  _Buffer_ must already be allocated.  If _Length_ exceeds
          the number of bytes allocated to _Buffer_, segmentation
          violations may happen.
*/


extern
String_Type
IO_Get_Line(IO_Type Conduit);
/*
   IO_Get_Line reads _Conduit_ until either a newline character is read
   or end-of-file is reached, and then returns a newly allocated string
   that contains the characters read in.  On error, this function 
   returns NULL.  It will also return NULL if the end-of-file is
   reached before any characters are read.  In other words, you can
   use this function to get the last line of a file that does not have
   a newline before the end-of-file.
  
   If there are more than 1023 characters on the line being read in
   from _Conduit_, only the first 1023 characters will be read in.
  
   Note:  This function returns a newly allocated string.  When this
          string is not needed anymore, it should be freed to
          conservere memory.
*/

extern
Integer_Type
IO_Put_Line(IO_Type Conduit, String_Type String);
/*
   IO_Put_Line writes the contents of _String_ to _Conduit_, followed
   by a newline character.
  
   This function returns -1 on error, 0 if _String_ is NULL, or
   the number of characters written on success.  The number of
   characters written on success will be strlen(_String_) + 1,
   the +1 due to the newline character being added.
*/

extern
IO_Descriptor_Type
IO_Descriptor_Of(IO_Type Handle);

#endif  /* IO_H */
