#if defined(__GNUC__)
#define SCCS SCCS __attribute__ ((__unused__))
#endif

static char *SCCS = "@(#) Bag.c (1.2), Last modified: 12:21:17 16 Oct 1995";

/* ------------------------------------------------------------------------- */

#if defined(NeXT)
#include <stdlib.h>		/* where malloc() is defined on the NeXT */
#else
#include <malloc.h>		/* where malloc() is defined elsewhere   */
#endif

#include <stdio.h>
#include <string.h>

#include <RL/Bag.h>
#include <RL/Tree.h>


/* ------------------------------------------------------------------------ */

/* Since I can't say "static const Integer_Type Hash_Size = 1023;" ... */

enum { Hash_Size = 1023 };

/* ------------------------------------------------------------------------ */

#if 0
static
Integer_Type
Hasher(String_Type Key)		/* produces a hash key */
{
  Integer_Type I      = strlen(Key);
  Integer_Type Result = 0;

  while (I >= 0)
  {
    Result += Key[I] * (I + 1); I--;
  }

  return (Result % Hash_Size);
}
#endif

/* ------------------------------------------------------------------------ */

typedef struct _Bag_Type
{
  Tree_Type      Tree[Hash_Size];
  Integer_Type   Size;
  void         (*Free_Item)(Bag_Item_Type Item);
} _Bag_Type;

/* ------------------------------------------------------------------------ */

static
Integer_Type
Compare(Tree_Item_Type Left, Tree_Item_Type Right)
{
  return (0);
}

Bag_Type
Bag_Allocate(void (*Free_Item)(Bag_Item_Type))
{
  Bag_Type     Bag = (Bag_Type) malloc(sizeof(_Bag_Type));
  Integer_Type I;
  
  for (I = 0; I < Hash_Size; I++)
  {
    Bag->Tree[I] = Tree_Allocate(Compare_Delegate(Compare), NULL); /* Fix this later */
  }
  
  Bag->Free_Item = Free_Item;
  Bag->Size      = 0;
  
  return (Bag);
}

/* ------------------------------------------------------------------------ */

void
Bag_Deallocate(Bag_Type Bag, Boolean_Type Free_Item)
{
  Integer_Type I;
  
  for (I = 0; I < Hash_Size; I++)
  {
    Tree_Deallocate(Bag->Tree[I]);
  }
  
  free(Bag);
}

/* ------------------------------------------------------------------------ */

void
Bag_Clear(Bag_Type Bag, Boolean_Type Free_Item)
{
  Integer_Type I;
  
  for (I = 0; I < Hash_Size; I++)
  {
    Tree_Clear(Bag->Tree[I]);
  }
  
  Bag->Size = 0;
}


/* ------------------------------------------------------------------------ */

Boolean_Type
Bag_Insert_Item(Bag_Type Bag, String_Type Key, Bag_Item_Type Item)
{
  return (False);
}


/* ------------------------------------------------------------------------ */

Boolean_Type
Bag_Replace_Item(Bag_Type Bag, String_Type Key, Bag_Item_Type Item)
{
  return (False);
}

/* ------------------------------------------------------------------------ */

Bag_Item_Type
Bag_Remove_Item(Bag_Type Bag, String_Type Key, Boolean_Type Free_Item)
{
  Bag_Type Baggie = NULL;
  return (Baggie);
}

/* ------------------------------------------------------------------------ */

Bag_Item_Type
Bag_Retrieve_Item(Bag_Type Bag, String_Type Key)
{
  Bag_Type Baggie = NULL;
  return (Baggie);
}

/* ------------------------------------------------------------------------ */

Boolean_Type
Bag_Item_Exists(Bag_Type Bag, String_Type Key)
{
  return (False);
}

/* ------------------------------------------------------------------------ */

Integer_Type
Bag_Size(Bag_Type Bag)
{
  if (Bag)
  {
    return (Bag->Size);
  }
  else
  {
    return (0);
  }
}

/* ------------------------------------------------------------------------ */

Boolean_Type
Bag_Is_Empty(Bag_Type Bag)
{
  if (Bag)
  {
    return (Bag->Size == 0);
  }
  else
  {
    return (True);
  }
}

/* ------------------------------------------------------------------------ */

/* Bag_Iterate() ??? */
