/* SCCS: @(#) Common_Delegates.h (1.1), Last modified: 14:53:18 13 Sep 1995 */

#if !defined(COMMON_DELEGATES_H)
#define COMMON_DELEGATES_H

typedef enum
{
  NULL_DELEGATE,
  ALLOCATE_DELEGATE,
  COMPARE_DELEGATE,
  DEALLOCATE_DELEGATE,
  DUPLICATE_DELEGATE,
  LABEL_DELEGATE,
  READ_DELEGATE,
  WRITE_DELEGATE
} Delegate_Type;

#define No_Delegates                    NULL_DELEGATE
#define Allocate_Delegate(Allocate)     ALLOCATE_DELEGATE,   Allocate
#define Compare_Delegate(Compare)       COMPARE_DELEGATE,    Compare
#define Deallocate_Delegate(Deallocate) DEALLOCATE_DELEGATE, Deallocate
#define Duplicate_Delegate(Duplicate)   DUPLICATE_DELEGATE,  Duplicate
#define Label_Delegate(Label)           LABEL_DELEGATE,      Label
#define Read_Delegate(Read)             READ_DELEGATE,       Read
#define Write_Delegate(Write)           WRITE_DELEGATE,      Write

#endif /* COMMON_DELEGATES_H */
