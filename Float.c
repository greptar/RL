#if defined(__GNUC__)
#define SCCS SCCS __attribute__ ((__unused__))
#endif

static char *SCCS = "@(#) Float.c (1.1), Last modified: 14:53:49 13 Sep 1995";

/* ------------------------------------------------------------------------- */

#if defined(NeXT)
#include <stdlib.h>		/* where malloc() is defined on the NeXT */
#else
#include <malloc.h>		/* where malloc() is defined elsewhere   */
#endif

#include <stdio.h>		/* where NULL is defined on SunOS */

#include <RL/Array.h>
#include <RL/Float.h>

/* ------------------------------------------------------------------------ */

Float_Type *
Float_Allocate(void)
{
  return (malloc(sizeof(Float_Type)));
}

void
Float_Deallocate(Float_Type *Float)
{
  free(Float);
}

/* ------------------------------------------------------------------------ */

Float_Type *
Float_Read(Float_Type *Float, String_Type Name, SF_Type SF)
{
  if (Float == NULL)
  {
    Float_Type *New_Float = Float_Allocate();

    if (SF_Get_Float(SF, Name, New_Float))
      return (New_Float);
  }
  else
  {
    if (SF_Get_Float(SF, Name, Float))
      return (Float);
  }

  return (NULL);
}

Boolean_Type
Float_Write(Float_Type *Float, String_Type Name, SF_Type SF)
{
  return (SF_Put_Float(SF, Name, *Float));
}

/* ----------------------------------------------------------------------------------------------------------------------------- */

Float_1D_Type
Float_1D_Allocate(Integer_Type Length)
{
  return ((Float_1D_Type) Array_1D_Allocate(Length, sizeof(Float_Type)));
#if 0
  Float_1D_Type Result;

  Result = (Float_1D_Type) malloc(sizeof(Float_Type) * X);

  return (Result);
#endif
}

void
Float_1D_Deallocate(Float_1D_Type Array)
{
  Array_1D_Deallocate((Array_1D_Type) Array);
#if 0
  free(Array_1D);
#endif
}

/* ----------------------------------------------------------------------------------------------------------------------------- */

Float_2D_Type
Float_2D_Allocate(Integer_Type Length_1, Integer_Type Length_2)
{
  return ((Float_2D_Type) Array_2D_Allocate(Length_1, Length_2, sizeof(Float_Type)));
#if 0
  Integer_Type  I;
  Float_2D_Type Result;

  Result = (Float_2D_Type) malloc(sizeof(Float_1D_Type) * (X + 1));

  for (I = 0; I < X; I++)
  {
    Result[I] = (Float_1D_Type) malloc(sizeof(Float_Type) * Y);
  }

  Result[X] = NULL;		/* The end of the array */

  return (Result);
#endif
}

void
Float_2D_Deallocate(Float_2D_Type Array)
{
  Array_2D_Deallocate((Array_2D_Type) Array);
#if 0
  Integer_Type I;

  for (I = 0; Array_2D[I] != NULL; I++)
  {
    free(Array_2D[I]);
  }

  free(Array_2D);
#endif
}

/* ----------------------------------------------------------------------------------------------------------------------------- */

Float_3D_Type
Float_3D_Allocate(Integer_Type Length_1, Integer_Type Length_2, Integer_Type Length_3)
{
  return ((Float_3D_Type) Array_3D_Allocate(Length_1, Length_2, Length_3, sizeof(Float_Type)));
#if 0
  Integer_Type  I, J;
  Float_3D_Type Result;

  Result = (Float_3D_Type) malloc(sizeof(Float_2D_Type) * (X + 1));

  for (I = 0; I < X; I++)
  {
    Result[I] = (Float_2D_Type) malloc(sizeof(Float_1D_Type) * (Y + 1));

    for (J = 0; J < Y; J++)
    {
      Result[I][J] = (Float_1D_Type) malloc(sizeof(Float_Type) * Z);
    }

    Result[I][Y] = NULL;
  }

  Result[X] = NULL;

  return (Result);
#endif
}

void
Float_3D_Deallocate(Float_3D_Type Array)
{
  Array_3D_Deallocate((Array_3D_Type) Array);
#if 0
  Integer_Type I, J;

  for (I = 0; Array_3D[I] != NULL; I++)
  {
    for (J = 0; Array_3D[I][J] != NULL; J++)
    {
      free(Array_3D[I][J]);
    }

    free(Array_3D[I]);
  }

  free(Array_3D);
#endif
}
