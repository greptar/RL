#if defined(__GNUC__)
#define SCCS SCCS __attribute__ ((__unused__))
#endif

static char *SCCS = "@(#) Queue.c (1.1), Last modified: 14:53:38 13 Sep 1995";

/* ------------------------------------------------------------------------- */

#if defined(NeXT)
#include <stdlib.h>		/* where malloc() is defined on the NeXT */
#else
#include <malloc.h>		/* where malloc() is defined elsewhere   */
#endif

#include <stdarg.h>
#include <stdio.h>

#include <RL/List.h>
#include <RL/Queue.h>

typedef struct _Queue_Type
{
  List_Type                 Queue;
  Queue_Compare_Delegate    Compare;
  Queue_Deallocate_Delegate Deallocate;
} _Queue_Type;

Queue_Type
Queue_Allocate(Delegate_Type Delegate, ...)
{
  va_list    Argument_Pointer;
  Queue_Type Queue;

  Queue = (Queue_Type) malloc(sizeof(_Queue_Type));

  Queue->Compare = NULL;

  va_start(Argument_Pointer, Delegate);

  do
  {
    if (Delegate == NULL_DELEGATE)
    {
      break;
    }
    else
    {
      switch (Delegate)
      {
	Generic_Type Dummy;

      case COMPARE_DELEGATE:
	Queue->Compare = va_arg(Argument_Pointer, Queue_Compare_Delegate);       break;
      case DEALLOCATE_DELEGATE:
	Queue->Deallocate = va_arg(Argument_Pointer, Queue_Deallocate_Delegate); break;
      default:			/* Don't process other delegates */
	Dummy = va_arg(Argument_Pointer, Generic_Type);                          break;
      }
    }
  } while ((Delegate = va_arg(Argument_Pointer, Delegate_Type)) != NULL_DELEGATE);

  va_end(Argument_Pointer);

  Queue->Queue = List_Allocate(Compare_Delegate(Queue->Compare), NULL);

  return (Queue);
}

void
Queue_Deallocate(Queue_Type Queue)
{
  Queue_Clear(Queue);
  List_Deallocate(Queue->Queue);
  free(Queue);
}

void
Queue_Clear(Queue_Type Queue)
{
  Queue_Item_Type Item;

  if (Queue->Deallocate)
    while ((Item = List_Remove_Item_At_Head(Queue->Queue)) != NULL)
      Queue->Deallocate(Item);
  else
    List_Clear(Queue->Queue);
}

void
Queue_Enqueue_Item(Queue_Type Queue, Queue_Item_Type Item)
{
  if (Queue->Compare)		/* Priority Queue */
    /* List_Insert_Item(Queue->Queue, Item) */;
  else				/* Normal Queue */
    List_Append_Item(Queue->Queue, Item); /* Add at tail of list */
}

Queue_Item_Type
Queue_Dequeue_Item(Queue_Type Queue)
{
  return (List_Remove_Item_At_Head(Queue->Queue)); /* Remove from head of list */
}


Queue_Item_Type
Queue_Item_At_Head(Queue_Type Queue)
{
  return (List_Item_At_Head(Queue->Queue)); /* Head of list */
}

Boolean_Type
Queue_Is_Empty(Queue_Type Queue)
{
  return (List_Is_Empty(Queue->Queue));
}

Integer_Type
Queue_Length(Queue_Type Queue)
{
  return (List_Length(Queue->Queue));
}
