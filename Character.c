#if defined(__GNUC__)
#define SCCS SCCS __attribute__ ((__unused__))
#endif

static char *SCCS = "@(#) Character.c (1.1), Last modified: 14:53:51 13 Sep 1995";

/* ------------------------------------------------------------------------- */

#if defined(NeXT)
#include <stdlib.h>		/* where malloc() is defined on the NeXT */
#else
#include <malloc.h>		/* where malloc() is defined elsewhere   */
#endif

#include <stdio.h>		/* where NULL is defined on SunOS */

#include <RL/Character.h>

/* ------------------------------------------------------------------------ */

Character_Type *
Character_Allocate(void)
{
  return (malloc(sizeof(Character_Type)));
}

void
Character_Deallocate(Character_Type *Character)
{
  free(Character);
}

/* ------------------------------------------------------------------------ */
Character_Type *
Character_Read(Character_Type *Character, String_Type Name, SF_Type SF)
{
  if (Character == NULL)
  {
    Character_Type *New_Character = Character_Allocate();

    if (SF_Get_Character(SF, Name, New_Character))
      return (New_Character);
  }
  else
  {
    if (SF_Get_Character(SF, Name, Character))
      return (Character);
  }

  return (NULL);
}

Boolean_Type
Character_Write(Character_Type *Character, String_Type Name, SF_Type SF)
{
  return (SF_Put_Character(SF, Name, *Character));
}

