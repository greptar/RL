#if defined(__GNUC__)
#define SCCS SCCS __attribute__ ((__unused__))
#endif

static char *SCCS = "@(#) TS.c (1.1), Last modified: 14:53:28 13 Sep 1995";

/* ------------------------------------------------------------------------- */

#if defined(NeXT)
#include <stdlib.h>		/* where malloc() is defined on the NeXT */
#else
#include <malloc.h>		/* where malloc() is defined elsewhere   */
#endif

#include <stdio.h>

#include <RL/TS.h>

typedef struct _TS_Type
{
  Generic_Type *Items;
  Integer_Type  Current_Item;
  Integer_Type  Number_Of_Items;
} _TS_Type;


TS_Type
TS_Allocate(Integer_Type Number_Of_Items)
{
  Integer_Type I;
  TS_Type      TS     = (TS_Type) malloc(sizeof(_TS_Type));

  TS->Items           = (Generic_Type *) malloc(sizeof(Generic_Type) * Number_Of_Items);
  TS->Current_Item    = 0;
  TS->Number_Of_Items = Number_Of_Items;

  for (I = 0; I < Number_Of_Items; I++)
  {
    TS->Items[I] = (Generic_Type) NULL;
  }

  return (TS);
}


extern
void
TS_Deallocate(TS_Type TS)
{
  Integer_Type I;

  for (I = 0; I < TS->Number_Of_Items; I++)
    free(TS->Items[I]);
}


extern
Generic_Type
TS_Item(TS_Type TS, Integer_Type Size_Of_Item)
{
  Generic_Type S;

  if (TS->Items[TS->Current_Item] == NULL)
    TS->Items[TS->Current_Item] = (Generic_Type) malloc(Size_Of_Item);
  else
    TS->Items[TS->Current_Item] = (Generic_Type) realloc(TS->Items[TS->Current_Item], sizeof(Size_Of_Item));

  S = TS->Items[TS->Current_Item++]; TS->Current_Item = TS->Current_Item % TS->Number_Of_Items;

  return (S);
}
