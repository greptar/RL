#if defined(__GNUC__)
#define SCCS SCCS __attribute__ ((__unused__))
#endif

static char *SCCS = "@(#) Double.c (1.2), Last modified: 09:26:20 26 Oct 1995";

/* ------------------------------------------------------------------------- */

#if defined(NeXT)
#include <stdlib.h>		/* where malloc() is defined on the NeXT */
#else
#include <malloc.h>		/* where malloc() is defined elsewhere   */
#endif

#include <stdio.h>		/* where NULL is defined on SunOS */

#include <RL/Array.h>
#include <RL/Double.h>
#include <RL/Float.h>

/* ------------------------------------------------------------------------ */

Double_Type *
Double_Allocate(void)
{
  return (malloc(sizeof(Double_Type)));
}

void
Double_Deallocate(Double_Type *Double)
{
  free(Double);
}

/* ------------------------------------------------------------------------ */

Double_Type *
Double_Read(Double_Type *Double, String_Type Name, SF_Type SF)
{
  if (Double == NULL)
  {
    Float_Type  *Tmp_Float  = Float_Allocate();
    Double_Type *New_Double = Double_Allocate();

    if (SF_Get_Float(SF, Name, Tmp_Float))
    {
      *New_Double = *Tmp_Float;
      Float_Deallocate(Tmp_Float);
      return (New_Double);
    }
    else
    {
      Float_Deallocate(Tmp_Float);
      Double_Deallocate(New_Double);
    }
  }
  else
  {
    Float_Type  *Tmp_Float  = Float_Allocate();

    if (SF_Get_Float(SF, Name, Tmp_Float))
    {
      *Double = *Tmp_Float;
      Float_Deallocate(Tmp_Float);
      return (Double);
    }
    else
    {
      Float_Deallocate(Tmp_Float);
    }
  }

  return (NULL);
}

Boolean_Type
Double_Write(Double_Type *Double, String_Type Name, SF_Type SF)
{
  return (SF_Put_Float(SF, Name, *Double));
}

/* ----------------------------------------------------------------------------------------------------------------------------- */

Double_1D_Type
Double_1D_Allocate(Integer_Type Length)
{
  return ((Double_1D_Type) Array_1D_Allocate(Length, sizeof(Double_Type)));
#if 0
  Double_1D_Type Result;

  Result = (Double_1D_Type) malloc(sizeof(Double_Type) * X);

  return (Result);
#endif
}

void
Double_1D_Deallocate(Double_1D_Type Array)
{
  Array_1D_Deallocate((Array_1D_Type) Array);
#if 0
  free(Array_1D);
#endif
}

/* ----------------------------------------------------------------------------------------------------------------------------- */

Double_2D_Type
Double_2D_Allocate(Integer_Type Length_1, Integer_Type Length_2)
{
  return ((Double_2D_Type) Array_2D_Allocate(Length_1, Length_2, sizeof(Double_Type)));
#if 0
  Integer_Type  I;
  Double_2D_Type Result;

  Result = (Double_2D_Type) malloc(sizeof(Double_1D_Type) * (X + 1));

  for (I = 0; I < X; I++)
  {
    Result[I] = (Double_1D_Type) malloc(sizeof(Double_Type) * Y);
  }

  Result[X] = NULL;		/* The end of the array */

  return (Result);
#endif
}

void
Double_2D_Deallocate(Double_2D_Type Array)
{
  Array_2D_Deallocate((Array_2D_Type) Array);
#if 0
  Integer_Type I;

  for (I = 0; Array_2D[I] != NULL; I++)
  {
    free(Array_2D[I]);
  }

  free(Array_2D);
#endif
}

/* ----------------------------------------------------------------------------------------------------------------------------- */

Double_3D_Type
Double_3D_Allocate(Integer_Type Length_1, Integer_Type Length_2, Integer_Type Length_3)
{
  return ((Double_3D_Type) Array_3D_Allocate(Length_1, Length_2, Length_3, sizeof(Double_Type)));
#if 0
  Integer_Type  I, J;
  Double_3D_Type Result;

  Result = (Double_3D_Type) malloc(sizeof(Double_2D_Type) * (X + 1));

  for (I = 0; I < X; I++)
  {
    Result[I] = (Double_2D_Type) malloc(sizeof(Double_1D_Type) * (Y + 1));

    for (J = 0; J < Y; J++)
    {
      Result[I][J] = (Double_1D_Type) malloc(sizeof(Double_Type) * Z);
    }

    Result[I][Y] = NULL;
  }

  Result[X] = NULL;

  return (Result);
#endif
}

void
Double_3D_Deallocate(Double_3D_Type Array)
{
  Array_3D_Deallocate((Array_3D_Type) Array);
#if 0
  Integer_Type I, J;

  for (I = 0; Array_3D[I] != NULL; I++)
  {
    for (J = 0; Array_3D[I][J] != NULL; J++)
    {
      free(Array_3D[I][J]);
    }

    free(Array_3D[I]);
  }

  free(Array_3D);
#endif
}
