/* SCCS: @(#) Bag.h (1.1), Last modified: 14:53:25 13 Sep 1995 */

#if !defined(BAG_H)
#define BAG_H

/*
   CONCEPTS:
     A Bag is a ... (put description here).
*/

/* ------------------------------------------------------------------------ */

#include <RL/Common_Delegates.h>
#include <RL/Common_Types.h>

/* ------------------------------------------------------------------------ */

typedef struct _Bag_Type *Bag_Type;
typedef void             *Bag_Item_Type;

/* ------------------------------------------------------------------------ */

extern
Bag_Type
Bag_Allocate(void (*Free_Item)(Bag_Item_Type));

extern
void
Bag_Deallocate(Bag_Type Bag, Boolean_Type Free_Item);

extern
void
Bag_Clear(Bag_Type Bag, Boolean_Type Free_Item);

/* ------------------------------------------------------------------------ */

extern
Boolean_Type
Bag_Insert_Item(Bag_Type Bag, String_Type Key, Bag_Item_Type Item);
/*
  Returns True if the Item was successfully inserted, False otherwise.  An
  Item may not be inserted successfully if it already exists in the Bag.
*/

extern
Boolean_Type
Bag_Replace_Item(Bag_Type Bag, String_Type Key, Bag_Item_Type Item);
/*
*/

extern
Bag_Item_Type
Bag_Remove_Item(Bag_Type Bag, String_Type Key, Boolean_Type Free_Item);

extern
Bag_Item_Type
Bag_Retrieve_Item(Bag_Type Bag, String_Type Key);

/* ------------------------------------------------------------------------ */

extern
Boolean_Type
Bag_Item_Exists(Bag_Type Bag, String_Type Key);

extern
Integer_Type
Bag_Size(Bag_Type Bag);

extern
Boolean_Type
Bag_Is_Empty(Bag_Type Bag);

/* ------------------------------------------------------------------------ */

/* Bag_Iterate() ??? */

#endif /* BAG_H */
